<?php

namespace App\Tests\Entity;

use App\Entity\Edition;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class EditionTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): Edition
    {
        return (new Edition())
            ->setName('Edition 2020')
            ->setYear(2020)
            ->setNumberEditorTables(100)
            ->setNumberProtozoneTables(40)
            ->setNumberTableMaxByExhibitor(8)
            ->setEditorRegistration(true)
            ->setShopRegistration(true)
            ->setVolunteerRegistration(false)
            ->setPriceTable(65)
            ->setPriceSellingOption(100)
            ->setPricePresenter(45)
            ->setPercentageJeunePousse(80)
            ->setPriceProtozone(20)
            ->setPriceElectricityOption(70)
            ->setPriceMeal(6.5);
    }

    public function assertHasErrors(Edition $edition, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($edition);

        $messages = [];

        /** @var ConstraintViolation $errors */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }

        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidYearEntity()
    {
        // > 4
        $this->assertHasErrors($this->getEntity()->setYear(20201), 1);

        // < 4
        $this->assertHasErrors($this->getEntity()->setYear(20), 1);
    }

    public function testInvalidBlankNameEntity()
    {
        $this->assertHasErrors($this->getEntity()->setName(''), 1);
    }

    public function testInvalidDoubleYear()
    {
        $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Editions.yaml'
        ]);

        $this->assertHasErrors($this->getEntity()->setYear(2021), 1);
    }
}
