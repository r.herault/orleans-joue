<?php

namespace App\Tests\Entity;

use App\Entity\Estimate;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EstimateTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): Estimate
    {
        return (new Estimate())
            ->setName('Devis Mr. Doe')
            ->setHT('2700');
    }

    public function assertHasErrors(Estimate $estimate, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($estimate);

        $messages = [];

        /** @var ConstraintViolation $errors */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }

        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testValidTotalTTC()
    {
        $estimates = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Estimates.yaml'
        ]);

        $this->assertEquals(3240, $estimates['estimate']->getTotalTTC());
    }
}
