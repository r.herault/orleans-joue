<?php

namespace App\Tests\Entity;

use App\Entity\Proposition;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class PropositionTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): Proposition
    {
        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml',
        ]);

        $edition = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Editions.yaml'
        ]);

        return (new Proposition())
            ->setComment('Je ne veux pas être avec John')
            ->setNumberSingleBed(2)
            ->setNumberDoubleBed(3)
            ->setVolunteer($users['volunteer_user'])
            ->setEdition($edition['edition_2021']);
    }

    public function assertHasErrors(Proposition $proposition, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($proposition);

        $messages = [];

        /** @var ConstraintViolation $errors */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }

        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidBed()
    {
        $this->assertHasErrors($this->getEntity()->setNumberDoubleBed(-1), 1);

        $this->assertHasErrors($this->getEntity()->setNumberSingleBed(-1), 1);
    }

    public function testValidBlankComment()
    {
        $this->assertHasErrors($this->getEntity()->setComment(''), 0);
    }
}
