<?php

namespace App\Tests\Entity;

use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class UserTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): User
    {
        return (new User())
            ->setEmail('foo@bar.fr')
            ->setPassword('Azerty1234')
            ->setSurname('Foo')
            ->setName('Bar')
            ->setPhone('0682957564');
    }

    public function assertHasErrors(User $user, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($user);

        $messages = [];

        /** @var ConstraintViolation $errors */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }

        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidEmail()
    {
        $this->assertHasErrors($this->getEntity()->setEmail('aaaa'), 1);
        $this->assertHasErrors($this->getEntity()->setEmail('aaaa@a'), 1);
        $this->assertHasErrors($this->getEntity()->setEmail('@aaa.fr'), 1);
    }

    public function testInvalidPassword()
    {
        $this->assertHasErrors($this->getEntity()->setPassword('azerty'), 1);
        $this->assertHasErrors($this->getEntity()->setPassword('Aze1'), 1);
        $this->assertHasErrors($this->getEntity()->setPassword('azerty1234'), 1);
    }

    public function testValidPassword()
    {
        $this->assertHasErrors($this->getEntity()->setPassword('JohnDoe1'), 0);
        $this->assertHasErrors($this->getEntity()->setPassword('oZ7zoduDkcC2T8YMaty34N'), 0);
        $this->assertHasErrors($this->getEntity()->setPassword('Sp9QAzaQ^mkaJMjxSLo4!^'), 0);
        $this->assertHasErrors($this->getEntity()->setPassword('H&naLR$#X!Coz9W!&MN78$'), 0);
    }

    public function testInvalidPhone()
    {
        $this->assertHasErrors($this->getEntity()->setPhone('02'), 2);
        $this->assertHasErrors($this->getEntity()->setPhone('06948756981'), 2);
        $this->assertHasErrors($this->getEntity()->setPhone('aaaaaaa'), 2);
    }

    public function testInvalidSurname()
    {
        $this->assertHasErrors($this->getEntity()->setSurname(''), 2);
        $this->assertHasErrors($this->getEntity()->setSurname('a'), 1);
        $this->assertHasErrors($this->getEntity()->setSurname(str_repeat('a', 55)), 1);
    }

    public function testInvalidName()
    {
        $this->assertHasErrors($this->getEntity()->setName(''), 2);
        $this->assertHasErrors($this->getEntity()->setName('a'), 1);
        $this->assertHasErrors($this->getEntity()->setName(str_repeat('a', 55)), 1);
    }

    public function testInvalidDoubleEmail()
    {
        $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        $this->assertHasErrors($this->getEntity()->setEmail('user@orleans-joue.fr'), 1);
    }
}
