<?php

namespace App\Tests\Entity;

use App\Entity\Volunteer;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class VolunteerTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): Volunteer
    {
        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml',
        ]);

        return (new Volunteer())
            ->setAddress("16 rue d'Issoudun, 45160 Olivet")
            ->setHaveTeeShirt(false)
            ->setSizeTeeShirt("L")
            ->setUser($users['user_volunteer']);
    }

    public function assertHasErrors(Volunteer $volunteer, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($volunteer);

        $messages = [];

        /** @var ConstraintViolation $errors */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }

        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidSizeTeeShirt()
    {
        $this->assertHasErrors($this->getEntity()->setSizeTeeShirt("P"), 1);

        $this->assertHasErrors($this->getEntity()->setSizeTeeShirt(""), 1);
    }
}
