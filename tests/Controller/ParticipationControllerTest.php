<?php

namespace App\Tests\Controller;

use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ParticipationControllerTest extends WebTestCase
{

    use FixturesTrait;
    use NeedLogin;


    public function testParticipationIndexRedirectToLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/participation');
        $this->assertResponseRedirects('/login');
    }

    public function testLetAuthenticatedUserAccess()
    {
        $client = static::createClient();

        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Editions.yaml'
        ]);

        // Responsable login
        $this->login($client, $users['user_responsable']);

        $client->request('GET', '/participation');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Admin login
        $this->login($client, $users['user_admin']);

        $client->request('GET', '/participation');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testWrongRolesRedirectToLogin()
    {
        $client = static::createClient();

        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        $this->login($client, $users['user_volunteer']);

        $client->request('GET', '/participation');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        $this->login($client, $users['user_user']);

        $client->request('GET', '/participation');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }
}
