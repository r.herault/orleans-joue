<?php

namespace App\Tests\Controller;

use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PropositionControllerTest extends WebTestCase
{

    use FixturesTrait;
    use NeedLogin;

    public function testPropositionIndexRedirectToLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/proposition');
        $this->assertResponseRedirects('/login');
    }

    public function testLetAuthenticatedUserAccess()
    {
        $client = static::createClient();

        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        // Responsable login
        $this->login($client, $users['user_volunteer']);

        $client->request('GET', '/proposition');
        $this->assertResponseStatusCodeSame(Response::HTTP_MOVED_PERMANENTLY);
    }

    public function testWrongRolesRedirectToLogin()
    {
        $client = static::createClient();

        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        $this->login($client, $users['user_responsable']);

        $client->request('GET', '/proposition');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        $this->login($client, $users['user_user']);

        $client->request('GET', '/proposition');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

}
