<?php

namespace App\Tests\Controller;

use App\Tests\NeedLogin;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{

    use FixturesTrait;
    use NeedLogin;

    public function testH1OrleansJoue()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Orléans Joue');
    }

    /**
     * @dataProvider userProvider
     */
    public function testH1OrleansJoueWithAllRoles($user)
    {
        $client = static::createClient();

        $this->login($client, $user);

        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Orléans Joue');
    }

    public function userProvider()
    {
        $users = $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        yield [$users['user_user']];
        yield [$users['user_admin']];
        yield [$users['user_responsable']];
        yield [$users['user_user']];
    }
}
