<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class SecurityControllerTest extends WebTestCase
{

    use FixturesTrait;

    public function testDisplayLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        // $this->assertSelectorTextContains('h2', 'Connectez-vous à votre compte', $normalizeWhitespace = false);
        $this->assertSame('Connectez-vous à votre compte', $crawler->filter('h2')->text(null, true));
        $this->assertSelectorNotExists('.text-red-500');
    }

    public function testLoginWithBadCredentials()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $form = $crawler->selectButton('Se connecter')->form([
            'email' => 'user@orleans-joue.fr',
            'password' => 'fakepassword'
        ]);
        $client->submit($form);
        $this->assertResponseRedirects('/login');

        $client->followRedirect();
        $this->assertSelectorExists('.text-red-500');
    }

    public function testSuccessfullLogin()
    {
        $this->loadFixtureFiles([
            dirname(__DIR__) . '/fixtures/Users.yaml'
        ]);

        $client = static::createClient();

        $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('authenticate');
        $crawler = $client->request('POST', '/login', [
            'email' => 'user@orleans-joue.fr',
            'password' => 'Azerty1234',
            '_csrf_token' => $csrfToken
        ]);

        $this->assertResponseRedirects('/');
    }
}
