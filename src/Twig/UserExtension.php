<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;

class UserExtension extends AbstractExtension
{

  public function __construct(ManagerRegistry $doctrine) {
    $this->doctrine = $doctrine;
  }

  public function getFunctions(): array
  {
    return [
      new TwigFunction('userHasSociety', [$this, 'userHasSociety']),
      new TwigFunction('editionIsOpen', [$this, 'editionIsOpen']),
      new TwigFunction('editionExist', [$this, 'editionExist']),
      new TwigFunction('editionParticipationExist', [$this, 'editionParticipationExist']),
      new TwigFunction('editionPropositionExist', [$this, 'editionPropositionExist']),
      new TwigFunction('participationIsExist', [$this, 'participationIsExist']),
      new TwigFunction('participeEdition', [$this, 'participeEdition']),
      new TwigFunction('volunteerHasProposition', [$this, 'volunteerHasProposition']),
      new TwigFunction('activityEditionExist', [$this, 'activityEditionExist']),
    ];
  }

  public function userHasSociety(User $user) {
    return $user->hasSociety();
  }

  public function editionIsOpen() {
    $edition = $this->doctrine->getRepository(\App\Entity\Edition::class)->findOneBy(['year' => date("Y")], ['id' => 'DESC']);

    return $edition && $edition->getEditorRegistration();
  }

  public function editionExist() {
    $edition = $this->doctrine->getRepository(\App\Entity\Edition::class)->findOneBy(['year' => date("Y")], ['id' => 'DESC']);

    return $edition != null;
  }

  public function editionParticipationExist() {
    $edition = $this->doctrine->getRepository(\App\Entity\Edition::class)->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
    $participations = $this->doctrine->getRepository(\App\Entity\Participation::class)->findBy(['edition' => $edition]);

    return $participations != null;
  }

  public function editionPropositionExist() {
    $edition = $this->doctrine->getRepository(\App\Entity\Edition::class)->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
    $propositions = $this->doctrine->getRepository(\App\Entity\Proposition::class)->findBy(['edition' => $edition]);

    return $propositions != null;
  }

  public function participationIsExist(User $user) {
    $society = $user->getSociety();
    $participation = $society ? $society->getParticipation() : null;
    $date_participation = $participation ? $participation->getRegistration()->format('Y') : null;
    $date_now = new \DateTime();

    return $date_participation === $date_now->format('Y');
  }

  public function participeEdition() {
    $edition = $this->doctrine->getRepository(\App\Entity\Edition::class)->findOneBy(['year' => date("Y")], ['id' => 'DESC']);

    return $edition && $edition->getVolunteerRegistration();
  }

  public function volunteerHasProposition(User $user) {
    $volunteer = $this->doctrine->getRepository(\App\Entity\Volunteer::class)->findOneBy(['user' => $user]);
    $proposition = $this->doctrine->getRepository(\App\Entity\Proposition::class)->findOneBy(['volunteer' => $volunteer]);

    return $proposition != null;
  }

  public function activityEditionExist() {
      $edition = $this->doctrine->getRepository(\App\Entity\Edition::class)->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
      $activities = $this->doctrine->getRepository(\App\Entity\Activity::class)->findBy(['edition' => $edition]);

      return $activities != null;
  }
}
