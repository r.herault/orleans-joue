<?php

namespace App\Form;

use App\Entity\Participation;
use App\Entity\Edition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Expression;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ParticipationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('needSocket')
            ->add('sellingOption')
            ->add('numberTables', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('numberAnimationTable', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('numberMeal', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('comment')
            ->add('edition', EntityType::class, [
                'class' => Edition::class,
                'required' => true
            ])
            ->add('society')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participation::class,
            'translation_domain' => 'form.participation',
        ]);
    }
}
