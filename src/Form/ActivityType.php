<?php

namespace App\Form;

use App\Entity\Activity;
use App\Entity\Edition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('numberPeople', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('edition', EntityType::class, [
                'class' => Edition::class,
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Activity::class,
            'translation_domain' => 'form.activity'
        ]);
    }
}
