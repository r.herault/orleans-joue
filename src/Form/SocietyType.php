<?php

namespace App\Form;

use App\Entity\Society;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class SocietyType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name')
    ->add('logoFile', VichImageType::class, [
        'required' => false,
        'allow_delete' => false,
        'download_label' => 'Télécharger l\'image',
        'download_uri' => false,
        'image_uri' => true,
        'imagine_pattern' => 'thumb',
        'asset_helper' => true,
      ]
    )
    ->add('siret')
    ->add('address')
    ->add('exhibitor')
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => Society::class,
      'translation_domain' => 'form.society'
    ]);
  }
}
