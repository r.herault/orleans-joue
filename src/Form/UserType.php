<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('surname')
            ->add('name')
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Super admin' => 'ROLE_SUPER_ADMIN',
                    'Admin éditeurs' => 'ROLE_EDITEUR_ADMIN',
                    'Admin benevole' => 'ROLE_BENEVOLE_ADMIN',
                    'Admin protozone' => 'ROLE_PROTO_ADMIN',
                    'Responsable' => 'ROLE_RESPONSABLE',
                    'Salarié' => 'ROLE_SALARIE',
                    'Bénévole' => 'ROLE_BENEVOLE'
                ],
                'multiple' => true
            ])
            ->add('gardenParty')
            ->add('hosting')
            ->add('phone')
            ->add('society')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'form.user'
        ]);
    }
}
