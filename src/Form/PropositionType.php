<?php

namespace App\Form;

use App\Entity\Proposition;
use App\Entity\Edition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PropositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextareaType::class, [
                'required' => false
            ])
            ->add('numberSingleBed', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('numberDoubleBed', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('edition', EntityType::class, [
                'class' => Edition::class,
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Proposition::class,
            'translation_domain' => 'form.proposition',
        ]);
    }
}
