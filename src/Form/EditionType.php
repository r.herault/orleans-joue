<?php

namespace App\Form;

use App\Entity\Edition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PercentType;

class EditionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('editorRegistration')
            ->add('shopRegistration')
            ->add('volunteerRegistration')
            ->add('name')
            ->add('year', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('numberEditorTables', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('numberProtozoneTables', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('priceTable')
            ->add('priceSellingOption')
            ->add('pricePresenter')
            ->add('percentageJeunePousse', PercentType::class, [
                'symbol' => false,
                'type' => 'integer',
            ])
            ->add('priceProtozone')
            ->add('priceElectricityOption')
            ->add('priceMeal')
            ->add('numberTableMaxByExhibitor', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Edition::class,
            'translation_domain' => 'form.edition'
        ]);
    }
}
