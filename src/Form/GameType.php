<?php

namespace App\Form;

use App\Entity\Game;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('image1File', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_label' => 'Télécharger l\'image',
                'download_uri' => false,
                'image_uri' => true,
                'imagine_pattern' => 'thumb',
                'asset_helper' => true,
              ]
            )
            ->add('image2File', VichImageType::class, [
                'required' => false,
                'allow_delete' => false,
                'download_label' => 'Télécharger l\'image',
                'download_uri' => false,
                'image_uri' => true,
                'imagine_pattern' => 'thumb',
                'asset_helper' => true,
              ]
            )
            ->add('editor')
            ->add('type')
            ->add('date', DateType::class, [
                'years' => range(2000, date('Y') + 10)
            ])
            ->add('partyTime', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('minPlayer', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('maxPlayer', null, [
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('ageRequired', null, [
                'attr' => [
                    'min' => 0,
                    'max' => 100
                ]
            ])
            ->add('creators')
            ->add('description')
            ->add('link')
            ->add('distributor')
            ->add('price')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
            'translation_domain' => 'form.game',
        ]);
    }
}
