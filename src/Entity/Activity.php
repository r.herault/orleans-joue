<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActivityRepository")
 * @UniqueEntity(fields={"name"}, message="Une activité avec le même nom existe déjà")
 */
class Activity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberPeople;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Edition", inversedBy="activity")
     */
    private $edition;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActivityChoice", mappedBy="activity", orphanRemoval=true)
     */
    private $activityChoices;

    public function __construct()
    {
        $this->activityChoices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNumberPeople(): ?int
    {
        return $this->numberPeople;
    }

    public function setNumberPeople(int $numberPeople): self
    {
        $this->numberPeople = $numberPeople;

        return $this;
    }

    public function getEdition(): ?Edition
    {
        return $this->edition;
    }

    public function setEdition(?Edition $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection|ActivityChoice[]
     */
    public function getActivityChoices(): Collection
    {
        return $this->activityChoices;
    }

    public function addActivityChoice(ActivityChoice $activityChoice): self
    {
        if (!$this->activityChoices->contains($activityChoice)) {
            $this->activityChoices[] = $activityChoice;
            $activityChoice->setActivity($this);
        }

        return $this;
    }

    public function removeActivityChoice(ActivityChoice $activityChoice): self
    {
        if ($this->activityChoices->contains($activityChoice)) {
            $this->activityChoices->removeElement($activityChoice);
            // set the owning side to null (unless already changed)
            if ($activityChoice->getActivity() === $this) {
                $activityChoice->setActivity(null);
            }
        }

        return $this;
    }
}
