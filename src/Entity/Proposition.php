<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropositionRepository")
 * @UniqueEntity(
 *     fields={"volunteer", "edition"},
 *     errorPath="edition",
 *     message="Un volontaire ne peut pas s'inscrire deux fois à une même édition."
 * )
 */
class Proposition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $numberSingleBed;

    /**
     * @ORM\Column(type="integer")
     * @Assert\PositiveOrZero
     */
    private $numberDoubleBed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Volunteer", inversedBy="proposition")
     */
    private $volunteer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Edition", inversedBy="proposition")
     * @Assert\NotNull
     */
    private $edition;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ActivityChoice", mappedBy="proposition", orphanRemoval=true)
     */
    private $activityChoices;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Slot", mappedBy="proposition", orphanRemoval=true)
     */
    private $slots;

    public function __construct()
    {
        $this->activityChoices = new ArrayCollection();
        $this->slots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getNumberSingleBed(): ?int
    {
        return $this->numberSingleBed;
    }

    public function setNumberSingleBed(int $numberSingleBed): self
    {
        $this->numberSingleBed = $numberSingleBed;

        return $this;
    }

    public function getNumberDoubleBed(): ?int
    {
        return $this->numberDoubleBed;
    }

    public function setNumberDoubleBed(int $numberDoubleBed): self
    {
        $this->numberDoubleBed = $numberDoubleBed;

        return $this;
    }

    public function getMeal(): ?bool
    {
        return $this->meal;
    }

    public function setMeal(bool $meal): self
    {
        $this->meal = $meal;

        return $this;
    }

    public function getVolunteer(): ?Volunteer
    {
        return $this->volunteer;
    }

    public function setVolunteer(?Volunteer $volunteer): self
    {
        $this->volunteer = $volunteer;

        return $this;
    }

    public function getEdition(): ?Edition
    {
        return $this->edition;
    }

    public function setEdition(?Edition $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function __toString(): string
    {
        return $this->volunteer . ' ' . $this->edition;
    }

    /**
     * @return Collection|ActivityChoice[]
     */
    public function getActivityChoices(): Collection
    {
        return $this->activityChoices;
    }

    public function addActivityChoice(ActivityChoice $activityChoice): self
    {
        if (!$this->activityChoices->contains($activityChoice)) {
            $this->activityChoices[] = $activityChoice;
            $activityChoice->setProposition($this);
        }

        return $this;
    }

    public function removeActivityChoice(ActivityChoice $activityChoice): self
    {
        if ($this->activityChoices->contains($activityChoice)) {
            $this->activityChoices->removeElement($activityChoice);
            // set the owning side to null (unless already changed)
            if ($activityChoice->getProposition() === $this) {
                $activityChoice->setProposition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Slot[]
     */
    public function getSlots(): Collection
    {
        return $this->slots;
    }

    public function addSlot(Slot $slot): self
    {
        if (!$this->slots->contains($slot)) {
            $this->slots[] = $slot;
            $slot->setProposition($this);
        }

        return $this;
    }

    public function removeSlot(Slot $slot): self
    {
        if ($this->slots->contains($slot)) {
            $this->slots->removeElement($slot);
            // set the owning side to null (unless already changed)
            if ($slot->getProposition() === $this) {
                $slot->setProposition(null);
            }
        }

        return $this;
    }
}
