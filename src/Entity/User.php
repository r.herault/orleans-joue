<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="Il existe déjà un compte avec cette adresse mail")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\Regex("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d#?!@$%^&*-=\/]{8,}$/")
     * @Assert\NotCompromisedPassword
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your surname must be at least {{ limit }} characters long",
     *      maxMessage = "Your surname cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your name must be at least {{ limit }} characters long",
     *      maxMessage = "Your name cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Society", inversedBy="user")
     * @ORM\JoinColumn(name="society_id", referencedColumnName="id", nullable=true)
     */
    private $society;

    /**
     * @var string token for forgotten password
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $resetToken;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gardenParty;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hosting;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\Length(
     *      min = 10,
     *      max = 10,
     *      minMessage = "Your phone must be at least {{ limit }} characters long",
     *      maxMessage = "Your phone cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false
     * )
     * @Assert\Regex("/^0[1-9][0-9]{8}$/")
     */
    private $phone;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Add role to user
     * @param  string $role [user role like ROLE_USER]
     */
    public function addRole(string $role): self {
      $user_roles = $this->roles;
      if (!in_array($role, $user_roles)) $user_roles[] = $role;

      $this->roles = $user_roles;

      return $this;
    }

    /**
     * Remove role to user
     * @param  string $role [user role like ROLE_USER]
     */
    public function removeRole(string $role): self {
      $user_roles = $this->roles;

      if (($key = array_search($role, $user_roles)) !== false) {
        unset($user_roles[$key]);
      }

      $this->roles = $user_roles;

      return $this;
    }

    public function getSociety(): ?Society
    {
        return $this->society;
    }

    public function setSociety(?Society $society): self
    {
        $this->society = $society;

        return $this;
    }

    /**
     * return true if the user is linked with society
     */
    public function hasSociety(): bool {
      $society = $this->society;

      return $society ? true : false;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getResetToken() : string
    {
        return $this->resetToken;
    }

    public function setResetToken(?string $resetToken) : void
    {
        $this->resetToken = $resetToken;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getGardenParty(): ?bool
    {
        return $this->gardenParty;
    }

    public function setGardenParty(bool $gardenParty): self
    {
        $this->gardenParty = $gardenParty;

        return $this;
    }

    public function getHosting(): ?bool
    {
        return $this->hosting;
    }

    public function setHosting(bool $hosting): self
    {
        $this->hosting = $hosting;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
