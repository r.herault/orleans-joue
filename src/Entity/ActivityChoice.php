<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActivityChoiceRepository")
 */
class ActivityChoice
{

    const CHOICES = [
        0 => "Non",
        1 => "Oui",
        -1 => "Sans avis"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $choice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proposition", inversedBy="activityChoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $proposition;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Activity", inversedBy="activityChoices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $activity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChoice(): ?int
    {
        return $this->choice;
    }

    public function setChoice(int $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    public function getChoiceValue(): ?string
    {
        return self::CHOICES[$this->choice];
    }

    public function getProposition(): ?Proposition
    {
        return $this->proposition;
    }

    public function setProposition(?Proposition $proposition): self
    {
        $this->proposition = $proposition;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;

        return $this;
    }

    public function __toString(): ?string
    {
        return $this->activity . ' -> ' . $this->getChoiceValue();
    }
}
