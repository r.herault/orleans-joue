<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 * @UniqueEntity(fields={"name"}, message="Un jeu avec le même nom existe déjà")
 * @Vich\Uploadable
 */
class Game implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image1;

    /**
     * @Vich\UploadableField(mapping="game_images", fileNameProperty="image1")
     * @Assert\File(
     *     mimeTypes = {"image/jpeg", "image/jpg", "image/png", "image/svg"},
     *     mimeTypesMessage = "Attention l'image n'est pas au bon format ! Formats autorisés : jpg, jpeg, png et svg"
     * )
     * @var File|null
     */
    private $image1File;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image2;

    /**
     * @Vich\UploadableField(mapping="game_images", fileNameProperty="image2")
     * @Assert\File(
     *     mimeTypes = {"image/jpeg", "image/jpg", "image/png", "image/svg"},
     *     mimeTypesMessage = "Attention l'image n'est pas au bon format ! Formats autorisés : jpg, jpeg, png et svg"
     * )
     * @var File|null
     */
    private $image2File;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $editor;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $type;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $distributor;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Animate", inversedBy="game")
     */
    private $animate;

    /**
     * @ORM\Column(type="integer")
     */
    private $partyTime;

    /**
     * @ORM\Column(type="integer")
     */
    private $minPlayer;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxPlayer;

    /**
     * @ORM\Column(type="integer")
     */
    private $ageRequired;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $creators;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage1(): ?string
    {
        return $this->image1;
    }

    public function setImage1(?string $image1): self
    {
        $this->image1 = $image1;

        return $this;
    }

    public function setImage1File(File $image = null)
    {
        $this->image1File = $image;

        if($image) {
            $this->setUpdatedAt(new \DateTime('now'));
        }
    }

    public function getImage1File()
    {
        return $this->image1File;
    }

    public function getImage2(): ?string
    {
        return $this->image2;
    }

    public function setImage2(?string $image2): self
    {
        $this->image2 = $image2;

        return $this;
    }

    public function setImage2File(File $image = null)
    {
        $this->image2File = $image;

        if($image) {
            $this->setUpdatedAt(new \DateTime('now'));
        }
    }

    public function getImage2File()
    {
        return $this->image2File;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function setEditor(?string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getDistributor(): ?string
    {
        return $this->distributor;
    }

    public function setDistributor(string $distributor): self
    {
        $this->distributor = $distributor;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAnimate(): ?Animate
    {
        return $this->animate;
    }

    public function setAnimate(?Animate $animate): self
    {
        $this->animate = $animate;

        return $this;
    }

    public function getPartyTime(): ?int
    {
        return $this->partyTime;
    }

    public function setPartyTime(int $partyTime): self
    {
        $this->partyTime = $partyTime;

        return $this;
    }

    public function getMinPlayer(): ?int
    {
        return $this->minPlayer;
    }

    public function setMinPlayer(int $minPlayer): self
    {
        $this->minPlayer = $minPlayer;

        return $this;
    }

    public function getMaxPlayer(): ?int
    {
        return $this->maxPlayer;
    }

    public function setMaxPlayer(int $maxPlayer): self
    {
        $this->maxPlayer = $maxPlayer;

        return $this;
    }

    public function getAgeRequired(): ?int
    {
        return $this->ageRequired;
    }

    public function setAgeRequired(int $ageRequired): self
    {
        $this->ageRequired = $ageRequired;

        return $this;
    }

    public function getCreators(): ?string
    {
        return $this->creators;
    }

    public function setCreators(string $creators): self
    {
        $this->creators = $creators;

        return $this;
    }

    /**
    * String representation of object
    * @link https://php.net/manual/en/serializable.serialize.php
    * @return string the string representation of the object or null
    * @since 5.1.0
    */
    public function serialize()
    {
      return serialize(array(
        $this->id,
        $this->logo,
      ));
    }

    /**
    * Constructs the object
    * @link https://php.net/manual/en/serializable.unserialize.php
    * @param string $serialized <p>
    * The string representation of the object.
    * </p>
    * @return void
    * @since 5.1.0
    */
    public function unserialize($serialized)
    {
      list (
        $this->id,
        $this->logo,
        ) = unserialize($serialized, array('allowed_classes' => false));
      }

      public function getUpdatedAt()
      {
          return $this->updatedAt;
      }

      public function setUpdatedAt(\DateTimeInterface $updatedAt)
      {
          $this->updatedAt = $updatedAt;

          return $this;
      }
}
