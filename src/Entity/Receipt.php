<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceiptRepository")
 */
class Receipt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $TVA;

    /**
     * @ORM\Column(type="integer")
     */
    private $HT;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalTTC;

    /**
     * @ORM\Column(type="datetime")
     */
    private $pay;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTVA(): ?float
    {
        return $this->TVA;
    }

    public function setTVA(float $TVA): self
    {
        $this->TVA = $TVA;

        return $this;
    }

    public function getHT(): ?int
    {
        return $this->HT;
    }

    public function setHT(int $HT): self
    {
        $this->HT = $HT;

        return $this;
    }

    public function getTotalTTC(): ?int
    {
        return $this->totalTTC;
    }

    public function setTotalTTC(int $totalTTC): self
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    public function getPay(): ?\DateTimeInterface
    {
        return $this->pay;
    }

    public function setPay(\DateTimeInterface $pay): self
    {
        $this->pay = $pay;

        return $this;
    }
}
