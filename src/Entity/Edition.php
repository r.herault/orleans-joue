<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EditionRepository")
 * @UniqueEntity(fields={"year"}, message="Il existe déjà une édition pour cette année là") 
 */
class Edition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $editorRegistration;

    /**
     * @ORM\Column(type="boolean")
     */
    private $shopRegistration;

    /**
     * @ORM\Column(type="boolean")
     */
    private $volunteerRegistration;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Regex("/^\d{4}$/")
     */
    private $year;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberEditorTables;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberProtozoneTables;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Activity", mappedBy="edition")
     */
    private $activity;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proposition", mappedBy="edition")
     */
    private $proposition;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Participation", mappedBy="edition")
     */
    private $participation;

    /**
     * @ORM\Column(type="float")
     */
    private $priceTable;

    /**
     * @ORM\Column(type="float")
     */
    private $priceSellingOption;

    /**
     * @ORM\Column(type="float")
     */
    private $pricePresenter;

    /**
     * @ORM\Column(type="float")
     */
    private $percentageJeunePousse;

    /**
     * @ORM\Column(type="float")
     */
    private $priceProtozone;

    /**
     * @ORM\Column(type="float")
     */
    private $priceElectricityOption;

    /**
     * @ORM\Column(type="float")
     */
    private $priceMeal;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberTableMaxByExhibitor;

    public function __construct()
    {
        $this->activity = new ArrayCollection();
        $this->proposition = new ArrayCollection();
        $this->participation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEditorRegistration(): ?bool
    {
        return $this->editorRegistration;
    }

    public function setEditorRegistration(bool $editorRegistration): self
    {
        $this->editorRegistration = $editorRegistration;

        return $this;
    }

    public function getShopRegistration(): ?bool
    {
        return $this->shopRegistration;
    }

    public function setShopRegistration(bool $shopRegistration): self
    {
        $this->shopRegistration = $shopRegistration;

        return $this;
    }

    public function getVolunteerRegistration(): ?bool
    {
        return $this->volunteerRegistration;
    }

    public function setVolunteerRegistration(bool $volunteerRegistration): self
    {
        $this->volunteerRegistration = $volunteerRegistration;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getNumberEditorTables(): ?int
    {
        return $this->numberEditorTables;
    }

    public function setNumberEditorTables(int $numberEditorTables): self
    {
        $this->numberEditorTables = $numberEditorTables;

        return $this;
    }

    public function getNumberProtozoneTables(): ?int
    {
        return $this->numberProtozoneTables;
    }

    public function setNumberProtozoneTables(int $numberProtozoneTables): self
    {
        $this->numberProtozoneTables = $numberProtozoneTables;

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivity(): Collection
    {
        return $this->activity;
    }

    public function addActivity(Activity $activity): self
    {
        if (!$this->activity->contains($activity)) {
            $this->activity[] = $activity;
            $activity->setEdition($this);
        }

        return $this;
    }

    public function removeActivity(Activity $activity): self
    {
        if ($this->activity->contains($activity)) {
            $this->activity->removeElement($activity);
            // set the owning side to null (unless already changed)
            if ($activity->getEdition() === $this) {
                $activity->setEdition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Proposition[]
     */
    public function getProposition(): Collection
    {
        return $this->proposition;
    }

    public function addProposition(Proposition $proposition): self
    {
        if (!$this->proposition->contains($proposition)) {
            $this->proposition[] = $proposition;
            $proposition->setEdition($this);
        }

        return $this;
    }

    public function removeProposition(Proposition $proposition): self
    {
        if ($this->proposition->contains($proposition)) {
            $this->proposition->removeElement($proposition);
            // set the owning side to null (unless already changed)
            if ($proposition->getEdition() === $this) {
                $proposition->setEdition(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Participation[]
     */
    public function getParticipation(): Collection
    {
        return $this->participation;
    }

    public function addParticipation(Participation $participation): self
    {
        if (!$this->participation->contains($participation)) {
            $this->participation[] = $participation;
            $participation->setEdition($this);
        }

        return $this;
    }

    public function removeParticipation(Participation $participation): self
    {
        if ($this->participation->contains($participation)) {
            $this->participation->removeElement($participation);
            // set the owning side to null (unless already changed)
            if ($participation->getEdition() === $this) {
                $participation->setEdition(null);
            }
        }

        return $this;
    }

    public function getPriceTable(): ?float
    {
        return $this->priceTable;
    }

    public function setPriceTable(float $priceTable): self
    {
        $this->priceTable = $priceTable;

        return $this;
    }

    public function getPriceSellingOption(): ?float
    {
        return $this->priceSellingOption;
    }

    public function setPriceSellingOption(float $priceSellingOption): self
    {
        $this->priceSellingOption = $priceSellingOption;

        return $this;
    }

    public function getPricePresenter(): ?float
    {
        return $this->pricePresenter;
    }

    public function setPricePresenter(float $pricePresenter): self
    {
        $this->pricePresenter = $pricePresenter;

        return $this;
    }

    public function getPercentageJeunePousse(): ?float
    {
        return $this->percentageJeunePousse;
    }

    public function setPercentageJeunePousse(float $percentageJeunePousse): self
    {
        $this->percentageJeunePousse = $percentageJeunePousse;

        return $this;
    }

    public function getPriceProtozone(): ?float
    {
        return $this->priceProtozone;
    }

    public function setPriceProtozone(float $priceProtozone): self
    {
        $this->priceProtozone = $priceProtozone;

        return $this;
    }

    public function getPriceElectricityOption(): ?float
    {
        return $this->priceElectricityOption;
    }

    public function setPriceElectricityOption(float $priceElectricityOption): self
    {
        $this->priceElectricityOption = $priceElectricityOption;

        return $this;
    }

    public function getPriceMeal(): ?float
    {
        return $this->priceMeal;
    }

    public function setPriceMeal(float $priceMeal): self
    {
        $this->priceMeal = $priceMeal;

        return $this;
    }

    public function getNumberTableMaxByExhibitor(): ?int
    {
        return $this->numberTableMaxByExhibitor;
    }

    public function setNumberTableMaxByExhibitor(int $numberTableMaxByExhibitor): self
    {
        $this->numberTableMaxByExhibitor = $numberTableMaxByExhibitor;

        return $this;
    }

    public function __toString(): string {
      return $this->getName() . ' (' . $this->getYear() . ')';
    }
}
