<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParticipationRepository")
 * @UniqueEntity(fields={"society"}, message="Un exposant ne peut pas s'inscrire plusieurs fois à une même édition")
 * @ORM\HasLifecycleCallbacks
 */
class Participation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $needSocket;

    /**
     * @ORM\Column(type="boolean")
     */
    private $sellingOption;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registration;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberTables;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberAnimationTable;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberMeal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Edition", inversedBy="participation")
     */
    private $edition;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Receipt", cascade={"persist", "remove"})
     */
    private $receipt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Estimate", cascade={"persist", "remove"})
     */
    private $estimate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Animate", mappedBy="participation")
     */
    private $animate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Society", inversedBy="participation")
     */
    private $society;

    public function __construct()
    {
        $this->animate = new ArrayCollection();
        $this->registration = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNeedSocket(): ?bool
    {
        return $this->needSocket;
    }

    public function setNeedSocket(bool $needSocket): self
    {
        $this->needSocket = $needSocket;

        return $this;
    }

    public function getSellingOption(): ?bool
    {
        return $this->sellingOption;
    }

    public function setSellingOption(bool $sellingOption): self
    {
        $this->sellingOption = $sellingOption;

        return $this;
    }

    public function getRegistration(): ?\DateTimeInterface
    {
        return $this->registration;
    }

    public function setRegistration(\DateTimeInterface $registration): self
    {
        $this->registration = $registration;

        return $this;
    }

    public function getNumberTables(): ?int
    {
        return $this->numberTables;
    }

    public function setNumberTables(int $numberTables): self
    {
        $this->numberTables = $numberTables;

        return $this;
    }

    public function getNumberAnimationTable(): ?int
    {
        return $this->numberAnimationTable;
    }

    public function setNumberAnimationTable(int $numberAnimationTable): self
    {
        $this->numberAnimationTable = $numberAnimationTable;

        return $this;
    }

    public function getNumberMeal(): ?int
    {
        return $this->numberMeal;
    }

    public function setNumberMeal(int $numberMeal): self
    {
        $this->numberMeal = $numberMeal;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getEdition(): ?Edition
    {
        return $this->edition;
    }

    public function setEdition(?Edition $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function getReceipt(): ?Receipt
    {
        return $this->receipt;
    }

    public function setReceipt(?Receipt $receipt): self
    {
        $this->receipt = $receipt;

        return $this;
    }

    public function getEstimate(): ?Estimate
    {
        return $this->estimate;
    }

    public function setEstimate(?Estimate $estimate): self
    {
        $this->estimate = $estimate;

        return $this;
    }

    /**
     * @return Collection|Animate[]
     */
    public function getAnimate(): Collection
    {
        return $this->animate;
    }

    public function addAnimate(Animate $animate): self
    {
        if (!$this->animate->contains($animate)) {
            $this->animate[] = $animate;
            $animate->setParticipation($this);
        }

        return $this;
    }

    public function removeAnimate(Animate $animate): self
    {
        if ($this->animate->contains($animate)) {
            $this->animate->removeElement($animate);
            // set the owning side to null (unless already changed)
            if ($animate->getParticipation() === $this) {
                $animate->setParticipation(null);
            }
        }

        return $this;
    }

    public function getSociety(): ?Society
    {
        return $this->society;
    }

    public function setSociety(?Society $society): self
    {
        $this->society = $society;

        return $this;
    }

    public function __toString(): String {
      return $this->registration->format('Y-m-d');
    }
}
