<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocietyRepository")
 * @UniqueEntity(fields={"siret"}, message="Une société existe déjà avec le même numéro de SIRET")
 * @Vich\Uploadable
 */
class Society implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @Vich\UploadableField(mapping="society_images", fileNameProperty="logo")
     * @Assert\File(
     *     mimeTypes = {"image/jpeg", "image/jpg", "image/png", "image/svg"},
     *     mimeTypesMessage = "Attention l'image n'est pas au bon format ! Formats autorisés : jpg, jpeg, png et svg"
     * )
     * @var File|null
     */
    private $logoFile;


    /**
     * @ORM\Column(type="string", length=15)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ExhibitorType", inversedBy="society")
     * @Assert\NotNull
     */
    private $exhibitor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="society")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Participation", mappedBy="society", cascade={"persist", "remove"})
     */
    private $participation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function setLogoFile(File $image = null)
    {
        $this->logoFile = $image;

        if($image) {
            $this->setUpdatedAt(new \DateTime('now'));
        }

    }

    public function getLogoFile()
    {
        return $this->logoFile;
    }


    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getExhibitor(): ?ExhibitorType
    {
        return $this->exhibitor;
    }

    public function setExhibitor(?ExhibitorType $exhibitor): self
    {
        $this->exhibitor = $exhibitor;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setSociety($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getSociety() === $this) {
                $user->setSociety(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
      return $this->getName();
    }

    public function getParticipation(): ?Participation
    {
      return $this->participation;
    }

    public function setParticipation(?Participation $participation): self
    {
      $this->participation = $participation;

      // set (or unset) the owning side of the relation if necessary
      $newSociety = null === $participation ? null : $this;
      if ($participation->getSociety() !== $newSociety) {
        $participation->setSociety($newSociety);
      }

      return $this;
    }

    /**
    * String representation of object
    * @link https://php.net/manual/en/serializable.serialize.php
    * @return string the string representation of the object or null
    * @since 5.1.0
    */
    public function serialize()
    {
      return serialize(array(
        $this->id,
        $this->logo,
      ));
    }

    /**
    * Constructs the object
    * @link https://php.net/manual/en/serializable.unserialize.php
    * @param string $serialized <p>
    * The string representation of the object.
    * </p>
    * @return void
    * @since 5.1.0
    */
    public function unserialize($serialized)
    {
      list (
        $this->id,
        $this->logo,
        ) = unserialize($serialized, array('allowed_classes' => false));
      }

      public function getUpdatedAt()
      {
          return $this->updatedAt;
      }

      public function setUpdatedAt(\DateTimeInterface $updatedAt)
      {
          $this->updatedAt = $updatedAt;

          return $this;
      }
}
