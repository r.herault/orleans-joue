<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EstimateRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Estimate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalTTC;

    /**
     * @ORM\Column(type="float")
     */
    private $TVA = 1.2;

    /**
     * @ORM\Column(type="float")
     */
    private $HT;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTotalTTC(): ?int
    {
        return $this->totalTTC;
    }

    public function setTotalTTC(int $total): self
    {
        $this->totalTTC = $total;

        return $this;
    }

    public function getTVA(): ?float
    {
        return $this->TVA;
    }

    public function setTVA(float $TVA): self
    {
        $this->TVA = $TVA;

        return $this;
    }

    public function getHT(): ?float
    {
        return $this->HT;
    }

    public function setHT(float $HT): self
    {
        $this->HT = $HT;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function estimatedTotal()
    {
        $this->totalTTC = $this->HT * $this->TVA;
    }

    public function __toString()
    {
        return $this->totalTTC . '';
    }

}
