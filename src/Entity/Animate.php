<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimateRepository")
 */
class Animate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAnimate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Game", mappedBy="animate")
     */
    private $game;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Participation", inversedBy="animate")
     */
    private $participation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     */
    private $animator;

    public function __construct()
    {
        $this->game = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsAnimate(): ?bool
    {
        return $this->isAnimate;
    }

    public function setIsAnimate(bool $isAnimate): self
    {
        $this->isAnimate = $isAnimate;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGame(): Collection
    {
        return $this->game;
    }

    public function addGame(Game $game): self
    {
        if (!$this->game->contains($game)) {
            $this->game[] = $game;
            $game->setAnimate($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->game->contains($game)) {
            $this->game->removeElement($game);
            // set the owning side to null (unless already changed)
            if ($game->getAnimate() === $this) {
                $game->setAnimate(null);
            }
        }

        return $this;
    }

    public function getParticipation(): ?Participation
    {
        return $this->participation;
    }

    public function setParticipation(?Participation $participation): self
    {
        $this->participation = $participation;

        return $this;
    }

    public function getAnimator(): ?User
    {
        return $this->animator;
    }

    public function setAnimator(?User $animator): self
    {
        $this->animator = $animator;

        return $this;
    }
}
