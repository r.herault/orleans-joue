<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VolunteerRepository")
 */
class Volunteer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="boolean")
     */
    private $haveTeeShirt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proposition", mappedBy="volunteer")
     */
    private $proposition;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     * @Assert\Choice({"XS", "S", "M", "L", "XL", "XXL"})
     */
    private $sizeTeeShirt;

    public function __construct()
    {
        $this->proposition = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getHaveTeeShirt(): ?bool
    {
        return $this->haveTeeShirt;
    }

    public function setHaveTeeShirt(bool $haveTeeShirt): self
    {
        $this->haveTeeShirt = $haveTeeShirt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Proposition[]
     */
    public function getProposition(): Collection
    {
        return $this->proposition;
    }

    public function addProposition(Proposition $proposition): self
    {
        if (!$this->proposition->contains($proposition)) {
            $this->proposition[] = $proposition;
            $proposition->setVolunteer($this);
        }

        return $this;
    }

    public function removeProposition(Proposition $proposition): self
    {
        if ($this->proposition->contains($proposition)) {
            $this->proposition->removeElement($proposition);
            // set the owning side to null (unless already changed)
            if ($proposition->getVolunteer() === $this) {
                $proposition->setVolunteer(null);
            }
        }

        return $this;
    }

    public function getSizeTeeShirt(): ?string
    {
        return $this->sizeTeeShirt;
    }

    public function setSizeTeeShirt(?string $sizeTeeShirt): self
    {
        $this->sizeTeeShirt = $sizeTeeShirt;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
