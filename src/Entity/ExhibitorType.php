<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExhibitorTypeRepository")
 */
class ExhibitorType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Society", mappedBy="exhibitor")
     */
    private $society;

    public function __construct()
    {
        $this->society = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Society[]
     */
    public function getSociety(): Collection
    {
        return $this->society;
    }

    public function addSociety(Society $society): self
    {
        if (!$this->society->contains($society)) {
            $this->society[] = $society;
            $society->setExhibitor($this);
        }

        return $this;
    }

    public function removeSociety(Society $society): self
    {
        if ($this->society->contains($society)) {
            $this->society->removeElement($society);
            // set the owning side to null (unless already changed)
            if ($society->getExhibitor() === $this) {
                $society->setExhibitor(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
      return $this->getName();
    }
}
