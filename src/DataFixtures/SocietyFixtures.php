<?php

namespace App\DataFixtures;

use App\Entity\Society;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class SocietyFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');

        // On créé 10 société
        for ($i = 0; $i < 10; $i++) {
            $society = (new Society())
                ->setName($faker->company)
                ->setLogo($faker->imageUrl())
                ->setSiret($faker->numerify(str_repeat('#', 14)))
                ->setAddress($faker->address);

            $manager->persist($society);
        }

        $manager->flush();
    }
}
