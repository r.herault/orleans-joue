<?php
namespace App\DataFixtures;

use App\Entity\ExhibitorType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ExhibitorTypeFixtures extends Fixture {

  public function load(ObjectManager $manager) {
    $exhibitor_name = [
      'Éditeur',
      'Escape Game',
      'Protozone',
    ];

    foreach ($exhibitor_name as $name) {
      $exhibitor = new ExhibitorType();
      $exhibitor->setName($name);
      $manager->persist($exhibitor);
    }

    $manager->flush();
  }
}
