<?php
namespace App\Service;

use App\Repository\EditionRepository;
use Doctrine\ORM\EntityManagerInterface;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class Helpers {

  private $em;
  private $container;
  private $editionRepository;

  public function __construct(EntityManagerInterface $em, ContainerInterface $container, EditionRepository $editionRepository) {
    $this->em = $em;
    $this->container = $container;
    $this->editionRepository = $editionRepository;
  }

  /**
   * Authenticate user with email
   * @param  string $user_email [user email]
   */
  public function authenticate(string $user_email) {
    $user = $this->em->getRepository('App:User')->findOneBy(array('email' => $user_email));

    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
    $this->container->get('security.token_storage')->setToken($token);
    $this->container->get('session')->set('_security_main', serialize($token));
  }

  public function getTablesLeft(string $choice) {
    $edition = $this->editionRepository->findOneBy(['year' => date('Y')]);
    $tablesLeft = 0;

    if($choice === 'Éditeur') {
        $numberTablesMax = $edition->getNumberEditorTables();
        $numberTables = 0;
        $participations = $edition->getParticipation()->toArray();

        foreach ($participations as $participation) {
            if($participation->getSociety()->getExhibitor()->getName() === 'Éditeur') {
                $numberTables += $participation->getNumberTables();
            }
        }

        $tablesLeft = $numberTablesMax - $numberTables;

    } else {
        $numberTablesMax = $edition->getNumberProtozoneTables();
        $numberTables = 0;
        $participations = $edition->getParticipation()->toArray();

        foreach ($participations as $participation) {
            if($participation->getSociety()->getExhibitor()->getName() === 'Protozone') {
                $numberTables += $participation->getNumberTables();
            }
        }

        $tablesLeft = $numberTablesMax - $numberTables;
    }

    return $tablesLeft;
  }
}
