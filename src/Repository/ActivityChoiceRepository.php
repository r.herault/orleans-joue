<?php

namespace App\Repository;

use App\Entity\ActivityChoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ActivityChoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActivityChoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActivityChoice[]    findAll()
 * @method ActivityChoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityChoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActivityChoice::class);
    }

    // /**
    //  * @return ActivityChoice[] Returns an array of ActivityChoice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActivityChoice
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAllActivities($value){
        return $this->createQueryBuilder('ac')
            ->where('p.volunteer = :val AND a.id = ac.activity')
            ->setParameter('val', $value)
            ->join('App\Entity\Activity', 'a', 'WITH', 'a.id = ac.activity')
            ->join('App\Entity\Proposition', 'p', 'WITH', 'p.id = ac.proposition')
            ->getQuery()
            ->getResult()
        ;
    }
}
