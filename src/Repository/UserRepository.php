<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllStaff($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE \'%ROLE_SALARIE%\' AND u.society = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }

    public function findAllStaffNotAnimating($society, $ids)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE \'%ROLE_SALARIE%\' AND u.society = :society AND u.id NOT IN (:ids)')
            ->setParameter('society', $society)
            ->setParameter('ids', $ids, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
            ->getQuery()
            ->getResult();
    }

    public function getReferent($society)
    {
        return $this->createQueryBuilder('u')
            ->where('u.roles LIKE \'%ROLE_RESPONSABLE%\' AND u.society = :society')
            ->setParameter('society', $society)
            ->getQuery()
            ->getResult();
    }

    public function getAllEditeurs(){
        return $this->createQueryBuilder('u')
            ->where('(u.roles LIKE \'%ROLE_SALARIE%\' OR u.roles LIKE \'%ROLE_RESPONSABLE%\') AND et.name NOT LIKE \'%Protozone%\'')
            ->join('App\Entity\Society', 's', 'WITH', 's.id = u.society')
            ->join('App\Entity\ExhibitorType', 'et', 'WITH', 's.exhibitor = et.id')
            ->getQuery()
            ->getResult();
    }

    public function getAllProto(){
        return $this->createQueryBuilder('u')
            ->where('(u.roles LIKE \'%ROLE_RESPONSABLE%\' OR u.roles LIKE \'%ROLE_SALARIE%\') AND et.name LIKE \'%Protozone%\'')
            ->join('App\Entity\Society', 's', 'WITH', 's.id = u.society')
            ->join('App\Entity\ExhibitorType', 'et', 'WITH', 's.exhibitor = et.id')
            ->getQuery()
            ->getResult();
    }
}
