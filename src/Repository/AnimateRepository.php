<?php

namespace App\Repository;

use App\Entity\Animate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Animate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Animate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Animate[]    findAll()
 * @method Animate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Animate::class);
    }

    // /**
    //  * @return Animate[] Returns an array of Animate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Animate
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getParticipationGames($participation)
    {
        return $this->createQueryBuilder('a')
            ->where('a.participation = :participation')
            ->setParameter('participation', $participation)
            ->join('App\Entity\Participation', 'p', 'WITH', 'a.participation = p.id')
            ->join('App\Entity\Game', 'g')
            ->getQuery()
            ->getResult();
    }
}
