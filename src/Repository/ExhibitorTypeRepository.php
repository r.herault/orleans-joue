<?php

namespace App\Repository;

use App\Entity\ExhibitorType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ExhibitorType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExhibitorType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExhibitorType[]    findAll()
 * @method ExhibitorType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExhibitorTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExhibitorType::class);
    }

    // /**
    //  * @return ExhibitorType[] Returns an array of ExhibitorType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExhibitorType
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
