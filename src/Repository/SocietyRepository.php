<?php

namespace App\Repository;

use App\Entity\Society;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Society|null find($id, $lockMode = null, $lockVersion = null)
 * @method Society|null findOneBy(array $criteria, array $orderBy = null)
 * @method Society[]    findAll()
 * @method Society[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocietyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Society::class);
    }

    // /**
    //  * @return Society[] Returns an array of Society objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Society
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getSocieties($id)
    {
        return $this->createQueryBuilder('s')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->join('App\Entity\ExhibitorType', 'et', 'WITH', 's.exhibitor = et.id')
            ->join('App\Entity\User', 'u', 'WITH', 'u.society = s.id')
            ->getQuery()
            ->getResult();
    }

    public function getProtoSocieties()
    {
        return $this->createQueryBuilder('s')
            ->where('et.name LIKE \'%Protozone%\'')
            ->join('App\Entity\ExhibitorType', 'et', 'WITH', 's.exhibitor = et.id')
            ->join('App\Entity\User', 'u', 'WITH', 'u.society = s.id')
            ->getQuery()
            ->getResult();
    }
}
