<?php

namespace App\Repository;

use App\Entity\Participation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Participation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participation[]    findAll()
 * @method Participation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participation::class);
    }

    /**
     * @return Participation[] Returns an array of Participation objects
     */
    public function findParticipationBySociety($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.society = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getSocietyParticipation($edition, $society)
    {
        return $this->createQueryBuilder('p')
            ->where('e.id = :edition AND s.id = :society')
            ->setParameter('edition', $edition)
            ->setParameter('society', $society)
            ->join('App\Entity\Edition', 'e', 'WITH', 'p.edition = e.id')
            ->join('App\Entity\Society', 's', 'WITH', 'p.society = s.id')
            ->getQuery()
            ->getResult();
    }
}
