<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200327142238 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity CHANGE edition_id edition_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin_comment CHANGE volunteer_id volunteer_id INT DEFAULT NULL, CHANGE society_id society_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE animate CHANGE participation_id participation_id INT DEFAULT NULL, CHANGE animator_id animator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate CHANGE total_ttc total_ttc INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game ADD updated_at DATETIME DEFAULT NULL, CHANGE animate_id animate_id INT DEFAULT NULL, CHANGE image1 image1 VARCHAR(255) DEFAULT NULL, CHANGE image2 image2 VARCHAR(255) DEFAULT NULL, CHANGE editor editor VARCHAR(100) DEFAULT NULL, CHANGE link link VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE participation CHANGE edition_id edition_id INT DEFAULT NULL, CHANGE receipt_id receipt_id INT DEFAULT NULL, CHANGE estimate_id estimate_id INT DEFAULT NULL, CHANGE society_id society_id INT DEFAULT NULL, CHANGE comment comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE proposition CHANGE volunteer_id volunteer_id INT DEFAULT NULL, CHANGE edition_id edition_id INT DEFAULT NULL, CHANGE comment comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE society CHANGE exhibitor_id exhibitor_id INT DEFAULT NULL, CHANGE logo logo VARCHAR(255) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE society_id society_id INT DEFAULT NULL, CHANGE reset_token reset_token VARCHAR(255) DEFAULT NULL, CHANGE garden_party garden_party TINYINT(1) DEFAULT NULL, CHANGE hosting hosting TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE volunteer CHANGE user_id user_id INT DEFAULT NULL, CHANGE size_tee_shirt size_tee_shirt VARCHAR(3) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity CHANGE edition_id edition_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE admin_comment CHANGE volunteer_id volunteer_id INT DEFAULT NULL, CHANGE society_id society_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE animate CHANGE participation_id participation_id INT DEFAULT NULL, CHANGE animator_id animator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate CHANGE total_ttc total_ttc INT DEFAULT NULL');
        $this->addSql('ALTER TABLE game DROP updated_at, CHANGE animate_id animate_id INT DEFAULT NULL, CHANGE image1 image1 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE image2 image2 VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE editor editor VARCHAR(100) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE link link VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE participation CHANGE edition_id edition_id INT DEFAULT NULL, CHANGE receipt_id receipt_id INT DEFAULT NULL, CHANGE estimate_id estimate_id INT DEFAULT NULL, CHANGE society_id society_id INT DEFAULT NULL, CHANGE comment comment VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE proposition CHANGE volunteer_id volunteer_id INT DEFAULT NULL, CHANGE edition_id edition_id INT DEFAULT NULL, CHANGE comment comment VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE society CHANGE exhibitor_id exhibitor_id INT DEFAULT NULL, CHANGE logo logo VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE society_id society_id INT DEFAULT NULL, CHANGE reset_token reset_token VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE garden_party garden_party TINYINT(1) DEFAULT \'NULL\', CHANGE hosting hosting TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE volunteer CHANGE user_id user_id INT DEFAULT NULL, CHANGE size_tee_shirt size_tee_shirt VARCHAR(3) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
