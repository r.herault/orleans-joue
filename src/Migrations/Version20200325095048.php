<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200325095048 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE volunteer (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, address VARCHAR(255) NOT NULL, have_tee_shirt TINYINT(1) NOT NULL, size_tee_shirt VARCHAR(3) DEFAULT NULL, UNIQUE INDEX UNIQ_5140DEDBA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exhibitor_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slot (id INT AUTO_INCREMENT NOT NULL, proposition_id INT NOT NULL, day VARCHAR(8) NOT NULL, start TIME NOT NULL, end TIME NOT NULL, INDEX IDX_AC0E2067DB96F9E (proposition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proposition (id INT AUTO_INCREMENT NOT NULL, volunteer_id INT DEFAULT NULL, edition_id INT DEFAULT NULL, comment VARCHAR(255) DEFAULT NULL, number_single_bed INT NOT NULL, number_double_bed INT NOT NULL, INDEX IDX_C7CDC3538EFAB6B1 (volunteer_id), INDEX IDX_C7CDC35374281A5E (edition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimate (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, total_ttc INT DEFAULT NULL, tva DOUBLE PRECISION NOT NULL, ht DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participation (id INT AUTO_INCREMENT NOT NULL, edition_id INT DEFAULT NULL, receipt_id INT DEFAULT NULL, estimate_id INT DEFAULT NULL, society_id INT DEFAULT NULL, need_socket TINYINT(1) NOT NULL, selling_option TINYINT(1) NOT NULL, registration DATETIME NOT NULL, number_tables INT NOT NULL, number_animation_table INT NOT NULL, number_meal INT NOT NULL, comment VARCHAR(255) DEFAULT NULL, INDEX IDX_AB55E24F74281A5E (edition_id), UNIQUE INDEX UNIQ_AB55E24F2B5CA896 (receipt_id), UNIQUE INDEX UNIQ_AB55E24F85F23082 (estimate_id), UNIQUE INDEX UNIQ_AB55E24FE6389D24 (society_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE receipt (id INT AUTO_INCREMENT NOT NULL, date DATETIME NOT NULL, name VARCHAR(50) NOT NULL, tva DOUBLE PRECISION NOT NULL, ht INT NOT NULL, total_ttc INT NOT NULL, pay DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity_choice (id INT AUTO_INCREMENT NOT NULL, activity_id INT NOT NULL, proposition_id INT NOT NULL, choice INT NOT NULL, UNIQUE INDEX UNIQ_93DDBA9281C06096 (activity_id), INDEX IDX_93DDBA92DB96F9E (proposition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, animate_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, image1 VARCHAR(255) DEFAULT NULL, image2 VARCHAR(255) DEFAULT NULL, editor VARCHAR(100) DEFAULT NULL, type VARCHAR(100) NOT NULL, date DATE NOT NULL, description LONGTEXT NOT NULL, link VARCHAR(255) DEFAULT NULL, distributor VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_232B318C36C94A58 (animate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE society (id INT AUTO_INCREMENT NOT NULL, exhibitor_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, logo VARCHAR(255) DEFAULT NULL, siret VARCHAR(15) NOT NULL, address VARCHAR(150) NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D6461F2E2326834 (exhibitor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activity (id INT AUTO_INCREMENT NOT NULL, edition_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, number_people INT NOT NULL, INDEX IDX_AC74095A74281A5E (edition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE edition (id INT AUTO_INCREMENT NOT NULL, editor_registration TINYINT(1) NOT NULL, shop_registration TINYINT(1) NOT NULL, volunteer_registration TINYINT(1) NOT NULL, name VARCHAR(50) NOT NULL, year INT NOT NULL, number_editor_tables INT NOT NULL, number_protozone_tables INT NOT NULL, price_table DOUBLE PRECISION NOT NULL, price_selling_option DOUBLE PRECISION NOT NULL, price_presenter DOUBLE PRECISION NOT NULL, percentage_jeune_pousse DOUBLE PRECISION NOT NULL, price_protozone DOUBLE PRECISION NOT NULL, price_electricity_option DOUBLE PRECISION NOT NULL, price_meal DOUBLE PRECISION NOT NULL, number_table_max_by_exhibitor INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE staff (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, email VARCHAR(255) NOT NULL, phone_number VARCHAR(12) NOT NULL, garden_party TINYINT(1) NOT NULL, hosting TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, society_id INT DEFAULT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(150) NOT NULL, surname VARCHAR(100) NOT NULL, name VARCHAR(100) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', reset_token VARCHAR(255) DEFAULT NULL, garden_party TINYINT(1) DEFAULT NULL, hosting TINYINT(1) DEFAULT NULL, phone VARCHAR(15) NOT NULL, INDEX IDX_8D93D649E6389D24 (society_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE admin_comment (id INT AUTO_INCREMENT NOT NULL, volunteer_id INT DEFAULT NULL, society_id INT DEFAULT NULL, comment VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5048D0E58EFAB6B1 (volunteer_id), UNIQUE INDEX UNIQ_5048D0E5E6389D24 (society_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE animate (id INT AUTO_INCREMENT NOT NULL, participation_id INT DEFAULT NULL, animator_id INT DEFAULT NULL, is_animate TINYINT(1) NOT NULL, INDEX IDX_E0A344D56ACE3B73 (participation_id), UNIQUE INDEX UNIQ_E0A344D570FBD26D (animator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE volunteer ADD CONSTRAINT FK_5140DEDBA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E2067DB96F9E FOREIGN KEY (proposition_id) REFERENCES proposition (id)');
        $this->addSql('ALTER TABLE proposition ADD CONSTRAINT FK_C7CDC3538EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES volunteer (id)');
        $this->addSql('ALTER TABLE proposition ADD CONSTRAINT FK_C7CDC35374281A5E FOREIGN KEY (edition_id) REFERENCES edition (id)');
        $this->addSql('ALTER TABLE participation ADD CONSTRAINT FK_AB55E24F74281A5E FOREIGN KEY (edition_id) REFERENCES edition (id)');
        $this->addSql('ALTER TABLE participation ADD CONSTRAINT FK_AB55E24F2B5CA896 FOREIGN KEY (receipt_id) REFERENCES receipt (id)');
        $this->addSql('ALTER TABLE participation ADD CONSTRAINT FK_AB55E24F85F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id)');
        $this->addSql('ALTER TABLE participation ADD CONSTRAINT FK_AB55E24FE6389D24 FOREIGN KEY (society_id) REFERENCES society (id)');
        $this->addSql('ALTER TABLE activity_choice ADD CONSTRAINT FK_93DDBA9281C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
        $this->addSql('ALTER TABLE activity_choice ADD CONSTRAINT FK_93DDBA92DB96F9E FOREIGN KEY (proposition_id) REFERENCES proposition (id)');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C36C94A58 FOREIGN KEY (animate_id) REFERENCES animate (id)');
        $this->addSql('ALTER TABLE society ADD CONSTRAINT FK_D6461F2E2326834 FOREIGN KEY (exhibitor_id) REFERENCES exhibitor_type (id)');
        $this->addSql('ALTER TABLE activity ADD CONSTRAINT FK_AC74095A74281A5E FOREIGN KEY (edition_id) REFERENCES edition (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E6389D24 FOREIGN KEY (society_id) REFERENCES society (id)');
        $this->addSql('ALTER TABLE admin_comment ADD CONSTRAINT FK_5048D0E58EFAB6B1 FOREIGN KEY (volunteer_id) REFERENCES volunteer (id)');
        $this->addSql('ALTER TABLE admin_comment ADD CONSTRAINT FK_5048D0E5E6389D24 FOREIGN KEY (society_id) REFERENCES society (id)');
        $this->addSql('ALTER TABLE animate ADD CONSTRAINT FK_E0A344D56ACE3B73 FOREIGN KEY (participation_id) REFERENCES participation (id)');
        $this->addSql('ALTER TABLE animate ADD CONSTRAINT FK_E0A344D570FBD26D FOREIGN KEY (animator_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE proposition DROP FOREIGN KEY FK_C7CDC3538EFAB6B1');
        $this->addSql('ALTER TABLE admin_comment DROP FOREIGN KEY FK_5048D0E58EFAB6B1');
        $this->addSql('ALTER TABLE society DROP FOREIGN KEY FK_D6461F2E2326834');
        $this->addSql('ALTER TABLE slot DROP FOREIGN KEY FK_AC0E2067DB96F9E');
        $this->addSql('ALTER TABLE activity_choice DROP FOREIGN KEY FK_93DDBA92DB96F9E');
        $this->addSql('ALTER TABLE participation DROP FOREIGN KEY FK_AB55E24F85F23082');
        $this->addSql('ALTER TABLE animate DROP FOREIGN KEY FK_E0A344D56ACE3B73');
        $this->addSql('ALTER TABLE participation DROP FOREIGN KEY FK_AB55E24F2B5CA896');
        $this->addSql('ALTER TABLE participation DROP FOREIGN KEY FK_AB55E24FE6389D24');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649E6389D24');
        $this->addSql('ALTER TABLE admin_comment DROP FOREIGN KEY FK_5048D0E5E6389D24');
        $this->addSql('ALTER TABLE activity_choice DROP FOREIGN KEY FK_93DDBA9281C06096');
        $this->addSql('ALTER TABLE proposition DROP FOREIGN KEY FK_C7CDC35374281A5E');
        $this->addSql('ALTER TABLE participation DROP FOREIGN KEY FK_AB55E24F74281A5E');
        $this->addSql('ALTER TABLE activity DROP FOREIGN KEY FK_AC74095A74281A5E');
        $this->addSql('ALTER TABLE volunteer DROP FOREIGN KEY FK_5140DEDBA76ED395');
        $this->addSql('ALTER TABLE animate DROP FOREIGN KEY FK_E0A344D570FBD26D');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C36C94A58');
        $this->addSql('DROP TABLE volunteer');
        $this->addSql('DROP TABLE exhibitor_type');
        $this->addSql('DROP TABLE slot');
        $this->addSql('DROP TABLE proposition');
        $this->addSql('DROP TABLE estimate');
        $this->addSql('DROP TABLE participation');
        $this->addSql('DROP TABLE receipt');
        $this->addSql('DROP TABLE activity_choice');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE society');
        $this->addSql('DROP TABLE activity');
        $this->addSql('DROP TABLE edition');
        $this->addSql('DROP TABLE staff');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE admin_comment');
        $this->addSql('DROP TABLE animate');
    }
}
