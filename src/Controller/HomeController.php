<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ActivityRepository;
use App\Repository\ActivityChoiceRepository;
use App\Repository\EditionRepository;
use App\Repository\PropositionRepository;
use App\Repository\SocietyRepository;
use App\Repository\UserRepository;
use App\Repository\VolunteerRepository;
use App\Repository\GameRepository;
use App\Repository\ParticipationRepository;
use App\Repository\SlotRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use App\Service\Helpers;

/**
 * @Route("/")
 */
class HomeController extends AbstractController
{
  /**
   * @Route("/", name="homepage")
   */
  public function index(UserRepository $userRepository, VolunteerRepository $volunteerRepository, ParticipationRepository $participationRepository, PropositionRepository $propositionRepository, EditionRepository $editionRepository, SocietyRepository $societyRepository, GameRepository $gameRepository, ActivityRepository $activityRepository, ActivityChoiceRepository $activityChoiceRepository, SlotRepository $slotRepository)
  {
    $user = null;
    $volonteer = null;
    $referent = null;
    $participation = null;
    $proposition = null;
    $edition = null;
    $society = null;
    $activities = null;
    $listeGames = null;
    $listeSalaries = null;
    $listeBenevoles = null;
    $listeEditeurs = null;
    $listeProtos = null;
    $listeActivites = null;
    $listeActivitesChoisies = null;
    $listeCreneaux = null;

    // On recherche l'édition dont l'année correspond à l'année en cours
    $edition = $editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
    if ($this->getUser() !== null) {
      $user = $userRepository->findOneBy(['id' => $this->getUser()->getId()]);

      if ($this->isGranted('ROLE_BENEVOLE')) {
        $volonteer = $volunteerRepository->findOneBy(['user' => $this->getUser()->getId()]);
        if (is_null($volonteer)) {
          $volonteer = ['address' => "Non renseignée", 'haveTeeShirt' => false];
        } else {
          if (!empty($edition)) {
            $proposition = $propositionRepository->getActualProposition($volonteer->getId());
            if (count($proposition) != 0) {
              $proposition = $proposition[0];
            } else {
              $proposition = null;
            }
            $listeActivites = $activityRepository->getAllActivities($volonteer->getId());
            $listeActivitesChoisies = $activityChoiceRepository->getAllActivities($volonteer->getId());
            $listeCreneaux = $slotRepository->getAllSlot($volonteer->getId());
          }
        }
      }

      if(!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE') && !$this->getUser()->getSociety()){
          return $this->redirectToRoute('user_profile_exposant');
      }

      if ($user->getSociety()) {
        $society = $societyRepository->findOneBy(['id' => $user->getSociety()->getId()]);
        if (!empty($edition)) {
          $participation = $participationRepository->getSocietyParticipation($edition->getId(), $society->getId());
          if (count($participation) != 0) {
            $participation = $participation[0];
            $listeGames = $gameRepository->getGames($participation->getId());
          } else {
            $participation = null;
          }
        }
        $referent = $userRepository->getReferent($society->getId())[0];
        $listeSalaries = $userRepository->findAllStaff($society->getId());
      }

      $listeBenevoles = $volunteerRepository->getAllBenevoles();

      $listeEditeurs = $userRepository->getAllEditeurs();

      $listeProtos = $userRepository->getAllProto();

      $activities = $activityRepository->findAll();

    }

    return $this->render('home/index.html.twig', [
      'user' => $user,
      'volonteer' => $volonteer,
      'participation' => $participation,
      'edition' => $edition,
      'society' => $society,
      'referent' => $referent,
      'activities' => $activities,
      'listeGames' => $listeGames,
      'listeSalaries' => $listeSalaries,
      'listeBenevoles' => $listeBenevoles,
      'listeEditeurs' => $listeEditeurs,
      'listeProtos' => $listeProtos,
      'proposition' => $proposition,
      'listeActivites' => $listeActivites,
      'listeActivitesChoisies' => $listeActivitesChoisies,
      'listeCreneaux' => $listeCreneaux,
    ]);
  }

  /**
   * @Route("/promote/{id}", name="promote")
   */
  public function promote(User $user, UserRepository $userRepository, Helpers $helpers)
  {
    $em = $this->getDoctrine()->getManager();

    $current_user = $this->getUser();
    $current_user->addRole('ROLE_SALARIE');
    $current_user->removeRole('ROLE_RESPONSABLE');

    $user_to_promote = $userRepository->find($user);
    $user_to_promote->addRole('ROLE_RESPONSABLE');
    $user_to_promote->removeRole('ROLE_SALARIE');

    $em->flush();

    $helpers->authenticate($current_user->getEmail());

    return $this->redirectToRoute('homepage');
  }
}
