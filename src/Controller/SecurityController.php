<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityController extends AbstractController
{
  /**
  * @Route("/login", name="app_login")
  */
  public function login(AuthenticationUtils $authenticationUtils): Response
  {
    if ($this->getUser()) {
      return $this->redirectToRoute('homepage');
    }

    // get the login error if there is one
    $error = $authenticationUtils->getLastAuthenticationError();
    // last username entered by the user
    $lastUsername = $authenticationUtils->getLastUsername();

    return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
  }

  /**
  * @Route("/register", name="app_register")
  */
  public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder) : Response {
    if($request->isMethod('POST')) {
      $user = new User();

      $user->setEmail($request->request->get('email'));
      $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
      $user->setName($request->request->get('name'));
      $user->setSurname($request->request->get('surname'));
      $user->setRoles(['ROLE_USER']);
      $user->setSociety(null);
      $user->setGardenParty(false);
      $user->setHosting(false);
      $user->setPhone($request->request->get('phone'));

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();
      $this->addFlash('success', 'Compte créé, veuillez vous connecter');

      return $this->redirectToRoute('app_login');
    }

    return $this->render('security/register.html.twig');
  }

  /**
  * @Route("/logout", name="app_logout")
  */
  public function logout() {
    return $this->redirectToRoute('homepage');
  }

  /**
  * @Route("/forgottenPassword", name="app_forgotten_password")
  */
  public function forgottenPassword(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator) : Response {
    if($request->isMethod('POST')) {
      $email = $request->request->get('email');

      $entityManager = $this->getDoctrine()->getManager();
      $user = $entityManager->getRepository(User::class)->findOneBy(['email' => $email]);

      if($user === null) {
        $this->addFlash('error', 'L\'adresse mail saisie est inconnue');
        return $this->redirectToRoute('app_forgotten_password');
      }

      $token = $tokenGenerator->generateToken();

      try {
        $user->setResetToken($token);
        $entityManager->flush();
      } catch(\Exception $e) {
        $this->addFlash('error', $e->getMessage());
        return $this->redirectToRoute('app_forgotten_password');
      }

      $url = $this->generateUrl('app_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

      $message = (new \Swift_Message('Mot de passe oublié'))
      ->setFrom('contact@orleans-joue.fr')
      ->setTo($user->getEmail())
      ->setBody(
        "Voici le lien pour mettre à jour votre mot de passe : " . $url,
        "text/html"
      );

      $mailer->send($message);

      $this->addFlash('success', 'Un mail vient de vous être envoyé. Merci de consulter vos mails');

      return $this->redirectToRoute('homepage');
    }

    return $this->render('security/forgotten_password.html.twig');
  }

  /**
  * @Route("/reset_password/{token}", name="app_reset_password")
  */
  public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder) {
    if($request->isMethod('POST')) {
      $entityManager = $this->getDoctrine()->getManager();

      $user = $entityManager->getRepository(User::class)->findOneBy(['resetToken' => $token]);

      if($user === null) {
        $this->addFlash('error', 'Le token est invalide. Merci de récupérer le lien dans le mail ou de cliquer de nouveau sur "Mot de passe oublié"');
        return $this->redirectToRoute('app_reset_password');
      }

      $user->setResetToken(null);

      $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
      $entityManager->flush();

      $this->addFlash('success', 'Votre mot de passe a bien été mis à jour. Vous pouvez maintenant vous connecter');

      return $this->redirectToRoute('homepage');
    } else {
      return $this->render('security/reset_password.html.twig', [
        'token' => $token
      ]);
    }
  }

}
