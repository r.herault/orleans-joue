<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Animate;
use App\Form\GameType;
use App\Repository\AnimateRepository;
use App\Repository\EditionRepository;
use App\Repository\GameRepository;
use App\Repository\ParticipationRepository;
use App\Repository\SocietyRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/game")
 */
class GameController extends AbstractController
{
    private $participation = null;

    public function getParticipation(ParticipationRepository $participationRepository, SocietyRepository $societyRepository, UserRepository $userRepository, EditionRepository $editionRepository)
    {
        $user = $userRepository->findOneBy(['id' => $this->getUser()->getId()]);
        $society = $societyRepository->getSocieties($user->getId());
        $edition = $editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        $participation = $participationRepository->getSocietyParticipation($edition->getId(), $society[0]->getId());

        if (count($participation) != 0) {
            $participation = $participation[0];
        } else {
            $participation = null;
        }
        return $participation;
    }

    /**
     * @Route("/", name="game_index", methods={"GET"})
     */
    public function index(AnimateRepository $animateRepository, ParticipationRepository $participationRepository, SocietyRepository $societyRepository, UserRepository $userRepository, EditionRepository $editionRepository, GameRepository $gameRepository): Response
    {
        $edition = $editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        if ($edition == null) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        } else {
            if(!$this->isGranted('ROLE_SUPER_ADMIN')){
                $participation = $this->getParticipation($participationRepository, $societyRepository, $userRepository, $editionRepository);
            }else{
                $participations = $participationRepository->findAll();
                $games = $gameRepository->findAll();
            }

            if(($this->isGranted('ROLE_SUPER_ADMIN') && !$edition->getEditorRegistration() && $games == null && $participations == null)  || (!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE') && !$edition->getEditorRegistration() && $participation == null)){
                return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
            }

            $games = null;
            $animates = $animateRepository->findAll();

            if($this->isGranted('ROLE_SUPER_ADMIN')){
                $games = $gameRepository->findAll();

                return $this->render('game/index.html.twig', [
                    'games' => $games,
                    'animates' => $animates,
                ]);
            } else {
                $games = $gameRepository->getGames($participation->getId());

                return $this->render('game/index.html.twig', [
                    'games' => $games,
                    'participation' => $participation,
                    'animates' => $animates,
                ]);
            }
        }
    }

    /**
     * @Route("/new", name="game_new", methods={"GET","POST"})
     */
    public function new(Request $request, ParticipationRepository $participationRepository, SocietyRepository $societyRepository, UserRepository $userRepository, EditionRepository $editionRepository): Response
    {
        $edition = $editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        if ($edition == null || $this->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        } else {
            $participation = $this->getParticipation($participationRepository, $societyRepository, $userRepository, $editionRepository);

            if($this->isGranted('ROLE_RESPONSABLE') && $participation == null || !$edition->getEditorRegistration()){
                return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
            }
        }

        $priceAnimation = $editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC'])->getPricePresenter();
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        $repo = $this->getDoctrine()->getManager()->getRepository(Animate::class);

        //on prépare la requete des animateurs qui n'ont pas encore de jeux
        $user = $userRepository->findOneBy(['id' => $this->getUser()->getId()]);
        $society = $societyRepository->findOneBy(['id' => $user->getSociety()->getId()]);
        $animates = $repo->findAll();
        $ids = array();
        foreach ($animates as $animate) {
            if ($animate->getAnimator() !== null) {
                array_push($ids, $animate->getAnimator()->getId());
            }
        }

        //Si l'array $ids est vide, la query ne marche pas
        array_push($ids, 0);

        $listeSalaries = $userRepository->findAllStaffNotAnimating($society->getId(), $ids);

        $animate = new Animate();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $animate->setIsAnimate(!$request->request->get('isAnimated'));
            $animate->setParticipation($this->getParticipation($participationRepository, $societyRepository, $userRepository, $editionRepository));
            if (!$request->request->get('isAnimated')) {
                if ($request->get('select_isAnimated') === "0") {
                    $animate->setAnimator($user);
                } else {
                    $animate->setAnimator($userRepository->findOneBy(['id' => $request->get('select_isAnimated')]));
                }
            } else {
                $animate->setAnimator(null);
            }

            $entityManager->persist($animate);
            $game->setAnimate($animate);

            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_index');
        }

        return $this->render('game/new.html.twig', [
            'game' => $game,
            'form' => $form->createView(),
            'animate' => $animate,
            'listeSalaries' => $listeSalaries,
            'user' => $user,
            'ids' => $ids,
            'priceAnimation' => $priceAnimation,
        ]);

    }

    /**
     * @Route("/{id}", name="game_show", methods={"GET"})
     */
    public function show(Game $game, ParticipationRepository $participationRepository, SocietyRepository $societyRepository, UserRepository $userRepository, EditionRepository $editionRepository): Response
    {
        $repo = $this->getDoctrine()->getRepository(Animate::class);
        $found = $repo->find($game->getAnimate());

        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $participation = $this->getParticipation($participationRepository, $societyRepository, $userRepository, $editionRepository);

            if ($participation->getId() != $found->getParticipation()->getId()) {
                return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
            }
        }

        return $this->render('game/show.html.twig', [
            'game' => $game,
            'animate' => $found,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="game_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Game $game, EntityManagerInterface $em, ParticipationRepository $participationRepository, SocietyRepository $societyRepository, UserRepository $userRepository, EditionRepository $editionRepository): Response
    {
        $priceAnimation = $editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC'])->getPricePresenter();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);
        $user = $userRepository->findOneBy(['id' => $this->getUser()->getId()]);

        $repo = $this->getDoctrine()->getManager()->getRepository(Animate::class);
        $found = $repo->find($game->getAnimate());

        if ($form->isSubmitted() && $form->isValid()) {
            if(!$this->isGranted('ROLE_SUPER_ADMIN')){
                $found->setIsAnimate(!$request->request->get('isAnimated'));
                if (!$request->request->get('isAnimated')) {
                    if ($request->get('select_isAnimated') === "0") {
                        $found->setAnimator($user);
                    } else {
                        $found->setAnimator($userRepository->findOneBy(['id' => $request->get('select_isAnimated')]));
                    }
                } else {
                    $found->setAnimator(null);
                }
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Jeu mis à jour');

            return $this->redirectToRoute('game_index');
        }

        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $participation = $this->getParticipation($participationRepository, $societyRepository, $userRepository, $editionRepository);

            //on prépare la requete des animateurs qui n'ont pas encore de jeux
            $user = $userRepository->findOneBy(['id' => $this->getUser()->getId()]);
            $society = $societyRepository->findOneBy(['id' => $user->getSociety()->getId()]);
            $animates = $repo->findAll();
            $ids = array();
            foreach ($animates as $animate) {
                if ($animate->getAnimator() !== null) {
                    array_push($ids, $animate->getAnimator()->getId());
                }
            }
            //Si l'array $ids est vide, la query ne marche pas
            array_push($ids, 0);
            $listeSalaries = $userRepository->findAllStaffNotAnimating($society->getId(), $ids);

            if ($participation->getId() != $found->getParticipation()->getId()) {
                return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
            }

            return $this->render('game/edit.html.twig', [
                'game' => $game,
                'form' => $form->createView(),
                'animate' => $found,
                'listeSalaries' => $listeSalaries,
                'user' => $user,
                'ids' => $ids,
                'priceAnimation' => $priceAnimation,
            ]);
        } else {

            return $this->render('game/edit.html.twig', [
                'game' => $game,
                'form' => $form->createView(),
                'animate' => $found,
                'priceAnimation' => $priceAnimation,
            ]);
        }
    }

    /**
     * @Route("/{id}", name="game_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Game $game): Response
    {
        $repo = $this->getDoctrine()->getRepository(Animate::class);
        $found = $repo->find($game->getAnimate()->getId());

        if ($this->isCsrfTokenValid('delete' . $game->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($found);
            $entityManager->remove($game);
            $entityManager->flush();
        }

        return $this->redirectToRoute('game_index');
    }
}
