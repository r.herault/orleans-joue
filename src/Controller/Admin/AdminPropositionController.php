<?php

namespace App\Controller\Admin;

use App\Repository\PropositionRepository;
use App\Repository\EditionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminPropositionController extends AbstractController
{
    /**
     * @Route("/admin/proposition", name="admin_proposition")
     */
    public function index(PropositionRepository $propositionRepository, EditionRepository $editionRepository)
    {

        $edition = $editionRepository->findOneBy(['year' => date('Y')]);

        return $this->render('admin/proposition/index.html.twig', [
            'propositions' => $propositionRepository->findBy(['edition' => $edition]),
            'edition' => $edition
        ]);
    }
}
