<?php

namespace App\Controller;

use App\Entity\Proposition;
use App\Entity\ActivityChoice;
use App\Entity\Slot;
use App\Form\PropositionType;
use App\Repository\ActivityRepository;
use App\Repository\ActivityChoiceRepository;
use App\Repository\VolunteerRepository;
use App\Repository\PropositionRepository;
use App\Repository\EditionRepository;
use App\Repository\SlotRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/proposition")
 */
class PropositionController extends AbstractController
{

    private $volunteerRepository;
    private $propositionRepository;
    private $editionRepository;
    private $slotRepository;

    public function __construct(VolunteerRepository $volunteerRepository, PropositionRepository $propositionRepository, EditionRepository $editionRepository, SlotRepository $slotRepository)
    {
        $this->volunteerRepository = $volunteerRepository;
        $this->propositionRepository = $propositionRepository;
        $this->editionRepository = $editionRepository;
        $this->slotRepository = $slotRepository;
    }

    /**
     * @Route("/", name="proposition_index", methods={"GET", "POST"})
     */
    public function proposition(Request $request, ActivityRepository $activityRepository, ActivityChoiceRepository $activityChoiceRepository): Response
    {
        $volunteer = $this->volunteerRepository->findOneBy(['user' => $this->getUser()]);
        $proposition = $this->propositionRepository->findOneBy(['volunteer' => $volunteer]);

        if($proposition && !$activityChoiceRepository->findOneBy(['proposition' => $proposition])){
            return $this->redirectToRoute('proposition_activities');
        } else if ($proposition && $activityChoiceRepository->findOneBy(['proposition' => $proposition]) && !$this->slotRepository->getAllSlot($volunteer)){
            return $this->redirectToRoute('proposition_availability');
        } else if($this->slotRepository->getAllSlot($volunteer)) {
            return $this->render('proposition/index.html.twig', [
                'proposition' => $this->propositionRepository->getActualProposition($volunteer)[0]
            ]);
        }

        $proposition = new Proposition();
        $form = $this->createForm(PropositionType::class, $proposition);
        $form->handleRequest($request);

        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        $activities = $activityRepository->findBy(['edition' => $edition]);
        if ($edition == null || $activities == null || ($this->isGranted('ROLE_SUPER_ADMIN') || !$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_BENEVOLE')) && !$edition->getVolunteerRegistration()) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        }

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();


            $proposition_exist = $this->propositionRepository->findOneBy(['volunteer' => $volunteer]);

            if($proposition_exist) {
                $this->addFlash("danger", "Un volontaire ne peut pas s'inscrire deux fois à une même édition.");
                return $this->redirectToRoute('homepage');
            }

            if(!$this->getUser()->getHosting()) {
                $proposition->setNumberDoubleBed(0);
                $proposition->setNumberSingleBed(0);
            }

            $proposition->setVolunteer($volunteer);

            $entityManager->persist($proposition);
            $entityManager->flush();

            return $this->redirectToRoute('proposition_activities');
        }

        return $this->render('proposition/proposition.html.twig', [
            'proposition' => $proposition,
            'form' => $form->createView(),
            'step' => 1
        ]);
    }

    /**
     * @Route("/activities", name="proposition_activities", methods={"GET", "POST"})
     */
    public function activity(Request $request, ActivityRepository $activityRepository): Response
    {
        $volunteer = $this->volunteerRepository->findOneBy(['user' => $this->getUser()]);
        $proposition = $this->propositionRepository->getActualProposition($volunteer);
        $entityManager = $this->getDoctrine()->getManager();


        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        $activities = $activityRepository->findBy(['edition' => $edition]);
        if ($edition == null || $activities == null || ($this->isGranted('ROLE_SUPER_ADMIN') || !$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_BENEVOLE')) && !$edition->getVolunteerRegistration() && $proposition == null) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        }

        if($request->isMethod('POST')) {
            foreach ($request->request->get('activity') as $activity => $choice) {

                $activity_choice = (new ActivityChoice())
                    ->setProposition($proposition[0])
                    ->setActivity($activityRepository->findOneBy(['name' => $activity]))
                    ->setChoice($choice);

                $entityManager->persist($activity_choice);
            }

            $entityManager->flush();

            return $this->redirectToRoute('proposition_availability');
        }

        if(empty($activityRepository->findBy(['edition' => $proposition[0]->getEdition()]))) {
            $entityManager->remove($proposition[0]);
            $entityManager->flush();

            $this->addFlash("danger", "Aucune activité n'a été ajoutée pour cette édition.");
            return $this->redirectToRoute('homepage');
        }

        return $this->render('proposition/activities.html.twig', [
            'activities' => $activityRepository->findBy(['edition' => $proposition[0]->getEdition()]),
            'step' => 2
        ]);

    }

    /**
     * @Route("/availability", name="proposition_availability", methods={"GET", "POST"})
     */
    public function availability(Request $request, ActivityRepository $activityRepository): Response
    {
        $volunteer = $this->volunteerRepository->findOneBy(['user' => $this->getUser()]);
        $proposition = $this->propositionRepository->getActualProposition($volunteer);
        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        $activities = $activityRepository->findBy(['edition' => $edition]);
        if ($edition == null || $activities == null || ($this->isGranted('ROLE_SUPER_ADMIN') || !$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_BENEVOLE')) && !$edition->getVolunteerRegistration() && $proposition == null) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        }

        if($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();

            $volunteer = $this->volunteerRepository->findOneBy(['user' => $this->getUser()]);
            $proposition = $this->propositionRepository->getActualProposition($volunteer);

            foreach($request->request->get('availability') as $day => $hours) {
                // Friday or monday
                if($day === 'friday' || $day === 'monday') {
                    foreach($hours as $period => $value) {
                        if($period === 'morning') {
                            $start = "8:00";
                            $end = "12:00";
                        } else {
                            $start = "14:00";
                            $end = "18:00";
                        }

                        $slot = (new Slot())
                            ->setDay($day)
                            ->setStart(\DateTime::createFromFormat('H:i', $start))
                            ->setEnd(\DateTime::createFromFormat('H:i', $end))
                            ->setProposition($proposition[0]);

                        $entityManager->persist($slot);
                    }

                } else {
                    // Saturday or sunday
                    $availability = [];
                    $last_hour = 0;
                    $index = 0;

                    foreach ($hours as $hour => $value) {
                        $index++;
                        $hour_sanitize = rtrim($hour, 'h');

                        if(sizeof($hours) < 3) {
                            $this->addFlash("danger", "Vous devez impérativement choisir des créneaux de 3h minimum.");
                            return $this->redirectToRoute('proposition_availability');
                        }

                        if($index > 1) {
                            if($hour_sanitize == $last_hour + 1 && $hour != key(array_slice($hours, -1, 1, true))) {
                                $availability[] = $hour_sanitize;
                            } else {
                                if($hour == key(array_slice($hours, -1, 1, true))) {
                                    $availability[] = $hour_sanitize;
                                }

                                if(reset($availability) + 3 > intval(end($availability) + 1)) {
                                    $this->addFlash("danger", "Vous devez impérativement choisir des créneaux de 3h minimum.");
                                    return $this->redirectToRoute('proposition_availability');
                                }

                                $start = reset($availability) . ':00';
                                $end = end($availability) + 1 . ':00';

                                $slot = (new Slot())
                                    ->setDay($day)
                                    ->setStart(\DateTime::createFromFormat('H:i', $start))
                                    ->setEnd(\DateTime::createFromFormat('H:i', $end))
                                    ->setProposition($proposition[0]);

                                $entityManager->persist($slot);

                                $index = 1;
                                $availability = [];
                                $availability[] = $hour_sanitize;
                                $last_hour = $hour_sanitize;
                            }
                        } else {
                            $availability[] = $hour_sanitize;
                        }

                        $last_hour = $hour_sanitize;
                    }
                }
            }

            $entityManager->flush();

            $this->addFlash("success", "Votre participation a bien été créée !");

            return $this->redirectToRoute('proposition_index');
        }

        return $this->render('proposition/availability.html.twig', [
            'step' => 3
        ]);
    }

    /**
     * @Route("/{id}/edit", name="proposition_edit", methods={"GET","POST"})
     */
    public function edit(Request $request): Response
    {
        if($request->isMethod('POST')) {
            dump($request);die();
        }

        $volunteer = $this->volunteerRepository->findOneBy(['user' => $this->getUser()]);
        $proposition = $this->propositionRepository->getActualProposition($volunteer)[0];

        return $this->render('proposition/edit.html.twig', [
            'proposition' => $proposition,
            'activities' => $proposition->getActivityChoices()->toArray(),
            'slots' => $proposition->getSlots()->toArray()
        ]);
    }

    /**
     * @Route("/{id}", name="proposition_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Proposition $proposition): Response
    {
        if ($this->isCsrfTokenValid('delete'.$proposition->getId(), $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();

            $activities = $proposition->getActivityChoices()->toArray();
            $slots = $proposition->getSlots()->toArray();

            foreach ($activities as $activity) {
                $entityManager->remove($activity);
            }

            foreach ($slots as $slot) {
                $entityManager->remove($slot);
            }

            $entityManager->remove($proposition);

            $entityManager->flush();
            if($this->isGranted('ROLE_SUPER_ADMIN')) {
                $this->addFlash("success", "Cette participation a bien été supprimée");
                return $this->redirectToRoute('admin_proposition');
            } else {
                $this->addFlash("success", "Votre participation a bien été supprimée");
                return $this->redirectToRoute('homepage');
            }
        }
    }
}
