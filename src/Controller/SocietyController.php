<?php

namespace App\Controller;

use App\Entity\Society;
use App\Form\SocietyType;
use App\Repository\SocietyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/society")
*/
class SocietyController extends AbstractController {

  /**
  * @Route("/{id}/edit", name="society_edit", methods={"GET","POST"})
  */
  public function edit(Request $request, Society $society, $id): Response {
    // empêche de modifier une société qui n'est pas la sienne
    $society_id = $this->getUser()->getSociety()->getId();
    $url_id = (int) $id;
    if ($society_id !== $url_id) {
      return $this->redirectToRoute('homepage');
    }

    $form = $this->createForm(SocietyType::class, $society);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->getDoctrine()->getManager()->flush();

      $this->addFlash('success', 'Les informations de votre société ont bien été mise à jour');
      return $this->redirectToRoute('user_profile');
    }

    return $this->render('user/society/edit.html.twig', [
      'society' => $society,
      'form' => $form->createView(),
    ]);
  }

  /**
  * @Route("/{id}", name="society_delete", methods={"DELETE"})
  */
  // TODO: Décommentez si on rajoute la fonctionnalité de suppression d'une société par un responsable
  // public function delete(Request $request, Society $society): Response
  // {
  //   if ($this->isCsrfTokenValid('delete'.$society->getId(), $request->request->get('_token'))) {
  //     $entityManager = $this->getDoctrine()->getManager();
  //     $entityManager->remove($society);
  //     $entityManager->flush();
  //   }
  //
  //   return $this->redirectToRoute('society_index');
  // }
}
