<?php

namespace App\Controller;

use App\Entity\Society;
use App\Entity\Volunteer;
use App\Form\SocietyType;
use App\Form\UserType;
use App\Form\UserProfileType;
use App\Form\VolunteerType;
use App\Repository\SocietyRepository;
use App\Repository\VolunteerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Service\Helpers;

/**
* @Route("/user")
*/
class UserController extends AbstractController
{
  /**
  * @Route("/profile", name="user_profile")
  */
  public function profile(Request $request, Helpers $helpers, VolunteerRepository $volunteerRepository): Response {
    $user = $this->getUser();
    $user_roles = $user->getRoles();
    $em = $this->getDoctrine()->getManager();

    // Si l'utilisateur a déjà une société on le redirige sur le profil global
    if ($user->hasSociety()) {
      $societyRepository = $em->getRepository(Society::class);

      return $this->render('user/profile/index.html.twig', [
        'societies' => $societyRepository->findAll(),
        'society' => $user->getSociety() ?: null,
      ]);
    }

    // On a donc pas de société

    // si on a remplis le formulaire pour choisir si on était exposant ou bénévole
    if($request->isMethod('POST')) {
      $role = strtoupper($request->request->get('role'));
      if (!in_array('ROLE_' . $role, $user_roles)) $user_roles[] = 'ROLE_' . $role;
      $user->setRoles($user_roles);

      $em = $this->getDoctrine()->getManager();
      $em->flush();

      $helpers->authenticate($user->getEmail());
    }

    // Si on est bénévole on renvoie le formulaire en question
    if($this->isGranted('ROLE_BENEVOLE')) {

      $entityManager = $this->getDoctrine()->getManager();
      $volunteer = $volunteerRepository->findOneBy(['user' => $user]);

      if($volunteer == null){
          return $this->redirectToRoute('volunteer_profile_edit', [
              'id' => $user->getid()
          ]);
      }

      return $this->render('user/profile/index.html.twig', [
        'volunteer' => $volunteer
      ]);

    } else if($this->isGranted('ROLE_RESPONSABLE') || $this->isGranted('ROLE_SALARIE')) {
      // rediriger form exposant
      return $this->redirectToRoute('user_profile_exposant');
    } else {
      // Afficher le formulaire de sélection de rôle
      return $this->render('user/choose_register.html.twig');
    }
  }


  /**
   * @Route("/volunteer/{id}/edit", name="volunteer_profile_edit", methods={"GET","POST"})
   */
  public function volunteer_profile_edit(Request $request, $id, VolunteerRepository $volunteerRepository): Response {
    $user = $this->getUser();
    $url_id = (int) $id;
    $volunteer = $volunteerRepository->findOneBy(['user' => $user]);

    if(!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_SALARIE') && $this->isGranted('ROLE_RESPONSABLE')){
        return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
    } else if ($volunteer != null && $volunteer->getId() !== $url_id) {
        return $this->redirectToRoute('homepage');
    }

    if($volunteer == null){
        $volunteer = new Volunteer();
        $volunteer->setUser($user);
    }

    $form = $this->createForm(VolunteerType::class, $volunteer);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->getDoctrine()->getManager()->persist($volunteer);
      $this->getDoctrine()->getManager()->flush();

      $this->addFlash('success', 'Votre profil a bien été mis à jour. N\'oubliez pas d\'indiquer si vous êtes présent à la garden party et si vous proposez un hébergement');
      return $this->redirectToRoute('user_profile');
    }

    return $this->render('user/benevole.html.twig', [
      'form' => $form->createView()
    ]);
  }


  /**
   * @Route("/profile/edit/{id}", name="user_profile_edit", methods={"GET","POST"})
   */
  public function profile_edit(Request $request, $id): Response {
    $user = $this->getUser();
    $url_id = (int) $id;

    if ($user->getId() !== $url_id) {
      return $this->redirectToRoute('homepage');
    }

    $form = $this->createForm(UserProfileType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $this->getDoctrine()->getManager()->flush();

      $this->addFlash('success', 'Votre profil a bien été mis à jour');
      return $this->redirectToRoute('user_profile');
    }

    return $this->render('user/profile/edit.html.twig', [
      'user' => $user,
      'form' => $form->createView(),
    ]);
  }

  /**
   * @Route("/profile/exposant", name="user_profile_exposant")
   */
  public function exposant(Request $request, SocietyRepository $societyRepository, Helpers $helpers): Response {
    if($this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_BENEVOLE')){
        return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
    }

    $society = new Society;
    $form = $this->createForm(SocietyType::class, $society);
    $form->handleRequest($request);
    $em = $this->getDoctrine()->getManager();
    $current_user = $this->getUser();

    // prevent access to user who already have a society
    if ($current_user->hasSociety()) return $this->redirectToRoute('user_profile');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

      // si une société est sélectionnée on la rejoint
      if ($_POST["company"] !== 'new') {
        $society_id = $_POST["company"];
        $society = $societyRepository->find($society_id);
        $current_user->setSociety($society);

        // on rend l'exposant salarié
        $current_user->addRole('ROLE_SALARIE');
        $current_user->removeRole('ROLE_RESPONSABLE');
        $em->flush();

        $helpers->authenticate($current_user->getEmail());
        $this->addFlash("success", "Vous avez bien rejoint <strong>" . $society->getName() . "</strong>, n'oubliez pas d'indiquer si vous êtes présent à la garden party et si vous avez besoin d'un hébergement");

        // sinon on crée une société et on la rejoint en tant que responsable
      } elseif ($form->isSubmitted() && $form->isValid()) {
        // On ajoute la nouvelle société et on la rejoint
        $em->persist($society);
        $current_user->setSociety($society);

        // on rend l'exposant responsable
        $current_user->addRole('ROLE_RESPONSABLE');
        $current_user->removeRole('ROLE_SALARIE');
        $em->flush();

        $helpers->authenticate($current_user->getEmail());
        $this->addFlash("success", "Vous avez bien créer <strong>" . $society->getName() . "</strong>, n'oubliez pas d'indiquer si vous êtes présent à la garden party et si vous avez besoin d'un hébergement");
      }

      return $this->redirectToRoute('user_profile');
    }

    // retrieves all compagnies
    $companies = $societyRepository->findAll();

    return $this->render('user/exposant.html.twig', [
      'form' => $form->createView(),
      'companies' => $companies,
    ]);
  }
}
