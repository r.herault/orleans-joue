<?php

namespace App\Controller;

use App\Entity\Participation;
use App\Entity\Estimate;
use App\Entity\Animate;
use App\Form\ParticipationType;
use App\Repository\ParticipationRepository;
use App\Repository\EditionRepository;
use App\Repository\GameRepository;
use App\Service\Helpers;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/participation")
 */
class ParticipationController extends AbstractController
{

    private $editionRepository;

    public function __construct(EditionRepository $editionRepository)
    {
        $this->editionRepository = $editionRepository;
    }

    /**
     * @Route("/", name="participation_index", methods={"GET"})
     */
    public function index(ParticipationRepository $participationRepository): Response
    {
        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $participations = $participationRepository->findAll();
            if ($edition == null || !$edition->getEditorRegistration() && $participations == null) {
                return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
            }

            return $this->render('participation/index.html.twig', [
                'participations' => $participations,
            ]);
        } else {
            $user = $this->getUser();
            $participation = $participationRepository->findBy(['society' => $user->getSociety()]);

            if ($edition == null || !$edition->getEditorRegistration() && $participation == null) {
                return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
            }


            return $this->render('participation/index.html.twig', [
                'participations' => $participation,
            ]);
        }
    }

    /**
     * @Route("/new", name="participation_new", methods={"GET","POST"})
     */
    public function new(Request $request, Helpers $helpers): Response
    {
        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        $participation = new Participation();
        $form = $this->createForm(ParticipationType::class, $participation);
        $form->handleRequest($request);

        if ($edition == null || !$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE') && !$edition->getEditorRegistration()) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE')) {
                $user = $this->getUser();
                $participation->setSociety($user->getSociety());
            }

            if (!$this->isGranted('ROLE_SUPER_ADMIN') && $helpers->getTablesLeft($user->getSociety()->getExhibitor()->getName()) < $form->getData()->getNumberTables()) {
                $this->addFlash("danger", "Il ne reste pas assez de tables. Tables restantes : " . $helpers->getTablesLeft($user->getSociety()->getExhibitor()->getName()));

                return $this->redirectToRoute('participation_index');
            }

            $entityManager = $this->getDoctrine()->getManager();

            $estimate = $this->calculateEstimation($participation);
            $participation->setEstimate($estimate);

            $entityManager->persist($participation);
            $entityManager->flush();
            $this->addFlash("success", "Participation créée avec succès");

            return $this->redirectToRoute('participation_index');
        }

        return $this->render('participation/new.html.twig', [
            'edition' => $edition,
            'participation' => $participation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="participation_show", methods={"GET"})
     */
    public function show(Participation $participation): Response
    {
        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        if (!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE') && !$edition->getEditorRegistration() && $participation == null) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        }

        return $this->render('participation/show.html.twig', [
            'participation' => $participation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="participation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Participation $participation, ParticipationRepository $participationRepository, GameRepository $gameRepository): Response
    {
        //$numberTables = $participationRepository->find($participation)->getNumberTables();*
        $nbGames = count($gameRepository->getGames($participation));
        $edition = $this->editionRepository->findOneBy(['year' => date("Y")], ['id' => 'DESC']);
        $form = $this->createForm(ParticipationType::class, $participation);
        $form->handleRequest($request);

        if (!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE') && !$edition->getEditorRegistration() && $participation == null) {
            return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_RESPONSABLE')) {
                $user = $this->getUser();
                $participation->setSociety($user->getSociety());
            }

            $entityManager = $this->getDoctrine()->getManager();

            $estimate = $this->calculateEstimation($participation);
            $participation->setEstimate($estimate);

            $entityManager->flush();
            $this->addFlash("success", "Participation modifiée avec succès");

            if ($request->request->get('participation')["numberTables"] < $nbGames) {
                return $this->redirectToRoute('choiceGames', ['id' => $participation->getId()]);
            }
            return $this->redirectToRoute('participation_index');
        }

        return $this->render('participation/edit.html.twig', [
            'edition' => $edition,
            'participation' => $participation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/games", name="choiceGames")
     */
    public function choiceGames(Participation $participation, Request $request, GameRepository $gameRepository)
    {
        $games = $gameRepository->getGames($participation);

        if (count($request->request->all()) > 0) {
            foreach ($request->request->all() as $gameId => $value) {
                $game = $gameRepository->find($gameId);

                $repo = $this->getDoctrine()->getRepository(Animate::class);
                $found = $repo->find($game->getAnimate()->getId());

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($found);
                $entityManager->remove($game);
                $entityManager->flush();
            }

            return $this->redirectToRoute('participation_index');
        }

        return $this->render('participation/games.html.twig', [
            'participation' => $participation,
            'games' => $games,
        ]);
    }

    /**
     * @Route("/{id}", name="participation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Participation $participation, GameRepository $gameRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $participation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($participation);
            $listeGames = $gameRepository->getGames($participation->getId());
            foreach ($listeGames as $game) {
                $repo = $this->getDoctrine()->getRepository(Animate::class);
                $found = $repo->find($game->getAnimate()->getId());

                $entityManager->remove($game);
                $entityManager->remove($found);
            }
            $entityManager->flush();
            $this->addFlash("success", "Participation supprimée avec succès");
        }

        return $this->redirectToRoute('participation_index');
    }


    private function calculateEstimation(Participation $participation)
    {
        $total = 0;

        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $isProtozone = $this->getUser()->getSociety()->getExhibitor()->getName() === 'Protozone';
            $priceTable = $isProtozone ? $participation->getEdition()->getPriceProtozone() : $participation->getEdition()->getPriceTable();
        } else {
            $priceTable = $participation->getEdition()->getPriceTable();
        }

        $total += $priceTable * $participation->getNumberTables();

        if ($participation->getSellingOption()) {
            $total += $participation->getEdition()->getPriceSellingOption();
        }
        if ($participation->getNeedSocket()) {
            $total += $participation->getEdition()->getPriceElectricityOption();
        }
        $total += $participation->getEdition()->getPriceMeal() * $participation->getNumberMeal();
        $total += $participation->getEdition()->getPricePresenter() * abs($participation->getNumberTables() - $participation->getNumberAnimationTable());


        $estimate = $participation->getEstimate();
        if($estimate == null){
            $estimate = (new Estimate())
                ->setName('DEVIS ' . $participation->getSociety()->getName() . ' (' . $participation->getEdition()->getName() . ')')
                ->setHT($total)
            ;
        } else {
            $estimate->setHT($total);
        }

        return $estimate;
    }

    /**
     * @Route("/ajax", name="_render_current_prices")
     * @param Request $request
     * @return Response
     */
    public function renderCurrentPrices(Request $request, EditionRepository $editionRepository) {
      if ($request->isXMLHttpRequest()) {
        $edition = $editionRepository->find($_POST["edition_id"]);
        $user = $this->getUser();
        $isProtozone = $user->getSociety()->getExhibitor()->getName() === 'Protozone';
        $priceTable = $isProtozone ? $edition->getPriceProtozone() : $edition->getPriceTable();

        $estimate = new Estimate();

        return new JsonResponse([
          'priceTable' => $priceTable,
          'priceAnimator' => $edition->getPricePresenter(),
          'priceMeal' => $edition->getPriceMeal(),
          'priceElectricityOption' => $edition->getPriceElectricityOption(),
          'priceSellingOption' => $edition->getPriceSellingOption(),
          'TVA' => $estimate->getTVA(),
        ]);
      }

      return $this->render('/bundles/TwigBundle/Exception/error403.html.twig', []);
    }
}
