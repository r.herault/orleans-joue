<?php

namespace App\EventListener;

use App\Entity\Participation;
use App\Entity\Estimate;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;

class ParticipationSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->estimate($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $changes = $args->getEntityChangeSet();
        $estimateValues = [
            'numberMeal',
            'numberAnimationTable',
            'numberTables',
            'sellingOption',
            'needSocket'
        ];

        foreach($changes as $key => $value) {
            if(in_array($key, $estimateValues)) {
                $this->estimate($args);
            }
        }
    }

    private function estimate($args) {
        $entity = $args->getObject();

        if (!$entity instanceof Participation) {
            return;
        }

        $entityManager = $args->getObjectManager();

        $invoice = 0;
        $invoice += $entity->getEdition()->getPriceTable() * $entity->getNumberTables();

        if($entity->getSellingOption()) {
            $invoice += $entity->getEdition()->getPriceSellingOption();
        }

        if($entity->getNeedSocket()) {
            $invoice += $entity->getEdition()->getPriceElectricityOption();
        }

        $invoice += $entity->getEdition()->getPriceMeal() * $entity->getNumberMeal();

        $invoice += $entity->getEdition()->getPricePresenter() * abs($entity->getNumberTables() - $entity->getNumberAnimationTable());

        $estimate = (new Estimate())
            ->setName('DEVIS ' . $entity->getSociety()->getName() . ' (' . $entity->getEdition()->getName() . ')')
            ->setHT($invoice);

        $entity->setEstimate($estimate);

        $entityManager->flush();
    }
}
