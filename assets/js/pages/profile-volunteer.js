import $ from 'jquery';

let $checkbox_tshirt = $('#volunteer_haveTeeShirt');

if($checkbox_tshirt.length > 0) {

  if($checkbox_tshirt.is(':checked')) {
    $('#select-size-tshirt').show();
  } else {
    $('#select-size-tshirt').hide();
  }


  $checkbox_tshirt.click(function() {
    $('#select-size-tshirt').toggle(this.checked);
  })
}
