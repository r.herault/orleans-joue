import $ from "jquery";

let $activities = $(".form-activities");

if ($activities.length > 0) {
  $(".choice-activities-yes").click(function() {
    $('.radio-activities[value="1"]').each(function() {
      $(this).prop("checked", true);
    });
  });

  $(".choice-activities-unknown").click(function() {
    $('.radio-activities[value="-1"]').each(function() {
      $(this).prop("checked", true);
    });
  });

  $(".choice-activities-no").click(function() {
    $('.radio-activities[value="0"]').each(function() {
      $(this).prop("checked", true);
    });
  });
}
