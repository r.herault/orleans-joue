import $ from "jquery";

if($(".form_exposant").length > 0) {
  let $form = $(".form_exposant");
  let $select = $('#select__company');
  let $submit = $('[type="submit"]');
  let $submit_text = $submit.text();

  // Désactive ou non le formulaire lors de la sélection d'une nouvelle société
  $select.on('change', function() {
    if ($(this).val() !== 'new') {
      $form.find('fieldset').prop('disabled', true);
      $form.find('.select-exhibitor')[0].selectize.disable();
      $submit.text('Rejoindre la société');
    } else {
      $form.find('fieldset').prop('disabled', false);
      $form.find('.select-exhibitor')[0].selectize.enable();
      $submit.text($submit_text);
    }
  });
}
