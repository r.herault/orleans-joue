import $ from 'jquery';

if($(".register-form").length > 0) {
  // setup disbled button
  $('.register-form').find('[type="submit"]').prop('disabled', true).addClass('opacity-50 cursor-not-allowed');

  // password validation
  $(document).on('keyup', 'input[type="password"]', function() {
    let password = $(this).val();

    // validate lower letter
    if (/[a-z]/.test(password)) {
      $('#letter').removeClass('invalid').addClass('valid');
    } else {
      $('#letter').removeClass('valid').addClass('invalid');
    }

    // validate capital letter
    if (/[A-Z]/.test(password)) {
      $('#capital').removeClass('invalid').addClass('valid');
    } else {
      $('#capital').removeClass('valid').addClass('invalid');
    }

    // validate number
    if (/\d/.test(password)) {
      $('#number').removeClass('invalid').addClass('valid');
    } else {
      $('#number').removeClass('valid').addClass('invalid');
    }

    // validate the length
    if (password.length >= 8) {
      $('#length').removeClass('invalid').addClass('valid');
    } else {
      $('#length').removeClass('valid').addClass('invalid');
    }

    if ($('#letter').hasClass('valid') && $('#capital').hasClass('valid') && $('#number').hasClass('valid') && $('#length').hasClass('valid')) {
      $('.register-form').find('[type="submit"]').prop('disabled', false).removeClass('opacity-50 cursor-not-allowed');
    } else {
      $('.register-form').find('[type="submit"]').prop('disabled', true).addClass('opacity-50 cursor-not-allowed');
    }
  });
}
