import $ from "jquery";

let nbTables = $("#nbTables").text();

$("#submitChoiceGames").click(function (event) {
    event.preventDefault();
    
    let games = $("#formChoiceGames>.groupjeu>input:not(:checked)");
    let gamesTotal = $("#formChoiceGames>.groupjeu>input");
    
    if (games.length > nbTables) {
        $('.messageError').show().css('display', 'inline-flex');
        $("#errorMsg").text("Il vous faut supprimer au moins " + (gamesTotal.length - nbTables) + " jeux.");
    }
    else {
        $("#formChoiceGames").submit();
    }
});