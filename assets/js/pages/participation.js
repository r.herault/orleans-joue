import $ from "jquery";

if($('.form-participation').length > 0) {

  $('.btn-info').click(function(e) {
    e.preventDefault();
    let $numberTables = $('#participation_numberTables').val();
    let $numberTablesAnimate = $('#participation_numberAnimationTable').val();

    if($numberTablesAnimate > $numberTables) {
      $('body').append('<div class="flash-message flash-error">Le nombre de tables animées ne peut pas être supérieur au nombre de tables voulues.</div>');
      $('.flash-message').fadeIn();
      setTimeout(() => {
        $('.flash-message').each(function() {
          $(this).fadeOut(300, () => {
            $(this).each(function() {
              $(this).remove();
            });
          });
        });
      }, 5000);
    } else {
      $('.form-participation').submit();
    }
  })

  /// partie update prix
  /// ============================
  var GLOBAL_PRICE = 0;

  getCurrentPrices();
  $('#participation_edition').on('change', () => { getCurrentPrices() });

  $(document).on('change', 'input', () => updatePrices());
}

$('#participation_numberAnimationTable').click(function(){
  $("#participation_numberAnimationTable").attr({
    'max' : $('#participation_numberTables').val()
  });
});

/// fonctions update prix
/// ============================
function getCurrentPrices() {

  $.ajax({
    url: "/participation/ajax",
    method: 'post',
    data: {
      'edition_id': $('select#participation_edition > option').val(),
    },
    success: function(data) {
      // global variable accessing from other functions
      window.priceTable = data.priceTable;
      window.priceAnimator = data.priceAnimator;
      window.priceMeal = data.priceMeal;
      window.priceElectricity = data.priceElectricity;
      window.priceSelling = data.priceSelling;
      window.TVA = data.TVA;
      GLOBAL_PRICE = 0;

      /// table value
      /// ============================
      let $value_table = $('input#participation_numberTables').val();
      let $price_table = $('.price-table');
      let $price_table_single = $('.price-table-single');

      if ($value_table == '') {
        $price_table.text("0");
        $price_table_single.text(data.priceTable);
      } else {
        $price_table.text(data.priceTable * $value_table);
        GLOBAL_PRICE += parseInt($price_table.text());
      }

      /// Animating table
      /// ============================
      let $animating_table = $('input#participation_numberAnimationTable');
      let $value_animating_table = $animating_table.val();

      $('.price-animating-table').text(data.priceAnimator);
      $animating_table.prop('max', $value_table);
      GLOBAL_PRICE += ($value_table - $value_animating_table) * data.priceAnimator;

      /// Meal
      /// ============================
      let $value_meal = $('input#participation_numberMeal').val();
      let $price_meal = $('.price-meal');
      let $price_meal_single = $('.price-meal-single');

      if ($value_meal == '') {
        $price_meal.text("0");
        $price_meal_single.text(data.priceMeal);
      } else {
        $price_meal.text(data.priceMeal * $value_meal);
        GLOBAL_PRICE += parseInt($price_meal.text());
      }

      /// Electicity
      /// ============================
      let $price_electricity = $('.price-electricity');
      $price_electricity.text(data.priceElectricityOption);

      if ($('#participation_needSocket').is(':checked')) {
        GLOBAL_PRICE += parseInt($price_electricity.text());
      }

      /// Selling option
      /// ============================
      let $price_selling = $('.price-selling');
      $price_selling.text(data.priceSellingOption);

      if ($('#participation_sellingOption').is(':checked')) {
        GLOBAL_PRICE += parseInt($price_selling.text());
      }

      /// Global price
      /// ============================
      $('.price-global-ht').text(GLOBAL_PRICE);
      $('.price-global-ttc').text(GLOBAL_PRICE * data.TVA);
    },
    error: function(error) {
      console.log(error);
    }
  });
}

function updatePrices() {
  GLOBAL_PRICE = 0;

  /// Table value
  /// ============================
  let $value_table = $('input#participation_numberTables').val();
  let $price_table = $('.price-table');

  if ($value_table == '') {
    $price_table.text(window.priceTable);
  } else {
    $price_table.text(window.priceTable * $value_table);
    GLOBAL_PRICE += parseInt($price_table.text());
  }

  /// Animating table
  /// ============================
  let $animating_table = $('input#participation_numberAnimationTable');
  let $value_animating_table = $('input#participation_numberAnimationTable').val();

  $animating_table.prop('max', $value_table);
  GLOBAL_PRICE += ($value_table - $value_animating_table) * window.priceAnimator;

  /// Meal
  /// ============================
  let $value_meal = $('input#participation_numberMeal').val();
  let $price_meal = $('.price-meal');

  if ($value_meal == '') {
    $price_meal.text(window.priceMeal);
  } else {
    $price_meal.text(window.priceMeal * $value_meal);
    GLOBAL_PRICE += parseInt($price_meal.text());
  }

  /// Electicity
  /// ============================
  let $price_electricity = $('.price-electricity');
  $price_electricity.text(window.priceElectricityOption);

  if ($('#participation_needSocket').is(':checked')) {
    GLOBAL_PRICE += parseInt($price_electricity.text());
  }

  /// Selling option
  /// ============================
  let $price_selling = $('.price-selling');
  $price_selling.text(window.priceSellingOption);

  if ($('#participation_sellingOption').is(':checked')) {
    GLOBAL_PRICE += parseInt($price_selling.text());
  }

  /// Global price
  /// ============================
  $('.price-global-ht').text(GLOBAL_PRICE);
  $('.price-global-ttc').text(GLOBAL_PRICE * window.TVA);
}
