import $ from "jquery";

/*To add pagination (10 pages) to a table :
<table>
    <tbody id='tbody%nomDiv%'>
        {% for e in range(0, liste|length - 1) %}
        {% set var = liste[e] %}
            <tr class={{e}}>
                {{var}}
            </tr>
    </tbody>
</table>
<div id="%nomDiv%" class="paginate">
    {% for i in range(1, (liste|length / 10)|round(0,"ceil")) %}
        <p>{{ i }}</p>
    {% endfor %}
</div>
*/

let paginates = $(".paginate");

paginates.each(function () {
    let id = $(this).attr('id');
    $("#" + id + ">p:first-child").addClass('selected');
    var list = [];
    for (var i = 0; i < 10; i++) {
        list.push(i);
    }
    $("#tbody" + id + ">*").each(function (tr) {
        if (!(parseInt($(this).attr('class')) in list)) {
            $(this).hide();
        }
    });

    $("#" + id + ">p").click(function () {
        $("#" + id + ">p").removeClass();
        $(this).addClass('selected');
        var newList = [];
        for (var i = (parseInt($(this).text()) - 1) * 10; i < parseInt($(this).text()) * 10; i++) {
            newList.push(i);
        }
        $("#tbody" + id + ">*").each(function () {
            if (newList.indexOf(parseInt($(this).attr('class'))) >= 0) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });
});