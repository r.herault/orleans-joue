import $ from 'jquery';
import selectize from 'selectize';
import 'selectize/dist/css/selectize.css';


let $select = $('#select_isAnimated');

if($select.length > 0) {
  $select.selectize();
}

let $checkbox_isAnimated = $('#isAnimated');

if($checkbox_isAnimated.is(':checked')) {
  $('#div_select_isAnimated').hide();
  $('#select_isAnimated').attr('required', false);
} else {
  $('#div_select_isAnimated').show();
  $('#select_isAnimated').attr('required', true);
}

$checkbox_isAnimated.click(function() {
  $('#select_isAnimated').attr('required', !$('#select_isAnimated').attr('required'));
  $('#div_select_isAnimated').toggle();
});

