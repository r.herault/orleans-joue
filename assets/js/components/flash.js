import $ from "jquery";

const flash_time = 8000;
let $flash_message = $(".flash-message");

if ($flash_message.length > 0) {
  $flash_message.each(function() {
    $(this).fadeIn();
  });

  setTimeout(() => {
    $flash_message.each(function() {
      $(this).fadeOut(300, () => {
        $(this).each(function() {
          $(this).remove();
        });
      });
    });
  }, flash_time);
}
