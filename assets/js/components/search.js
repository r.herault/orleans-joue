import $ from "jquery";

if ($("table[id^='searchTable'").length > 0) {

    $("input").keyup(function() {
        /*
        Pour utiliser la recherche, placez :
        <input type="text" name=":idTable" placeholder="Search something">
        Juste avant votre <table> en remplacant :idTable par l'id du tableau dans lequel faire la recherche
    */

    // Declare variables
    var input,
    filter,
    table,
    tr,
    tdSurname,
    tdName,
    i,
    txtValueSurname,
    txtValueName,
    surnameName,
    nameSurname,
    idTable;
    input = $(this);
    filter = input.val().toUpperCase();
    idTable = input.attr("name");
    table = document.getElementById(idTable);
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        tdSurname = tr[i].getElementsByTagName("td")[0];
        tdName = tr[i].getElementsByTagName("td")[1];
      if (tdSurname) {
          txtValueSurname = $.trim(tdSurname.innerText.toUpperCase());
          txtValueName = $.trim(tdName.innerText.toUpperCase());
          surnameName = txtValueSurname + " " + txtValueName;
          nameSurname = txtValueName + " " + txtValueSurname;
          if (
              (txtValueSurname + " " + txtValueName).indexOf(filter) > -1 ||
              (txtValueName + " " + txtValueSurname).indexOf(filter) > -1
              ) {
                  tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
}
