import $ from 'jquery';
import selectize from 'selectize';
import 'selectize/dist/css/selectize.css';


let $select = $('select');

if($select.length > 0) {
  $select.selectize();
}
