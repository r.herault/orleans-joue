// any CSS you import will output into a single css file (app.css in this case)
import '../scss/app.scss';

import $ from 'jquery';

import './components/flash';
import './components/form';
import './components/form_game.js';
import './components/search.js';
import './components/pagination.js';

import './pages/profile-volunteer';
import './pages/proposition';
import './pages/participation';
import './pages/choiceGames';

import './pages/form_exposant';
import './pages/register';
